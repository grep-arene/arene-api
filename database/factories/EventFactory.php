<?php

namespace Database\Factories;

use App\Http\Controllers\API\ProductController;
use App\Models\Event;
use Illuminate\Database\Eloquent\Factories\Factory;

class EventFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Event::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => 'Event ' . $this->faker->word,
            'image_path' => null,
            'date' => $this->faker->dateTimeThisYear->format('Y-m-d H:i:s'),
            'description' => $this->faker->text,
            'price' => $this->faker->randomFloat(2, 0, 99),
            'nb_place' => $this->faker->randomNumber(3),
        ];
    }

    public function image(): EventFactory
    {
        return $this->state(fn() => [
            'image_path' => ProductController::IMAGE_PATH . '/product_' . $this->faker->numberBetween(1, 32) . '.jpg',
        ]);
    }
}
