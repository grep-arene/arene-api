<?php

namespace Database\Factories;

use App\Http\Controllers\API\ProductController;
use App\Models\Game;
use Illuminate\Database\Eloquent\Factories\Factory;

class GameFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Game::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => 'Game ' . $this->faker->word,
            'image_path' => null,
        ];
    }

    public function image(): GameFactory
    {
        return $this->state(fn() => [
            'image_path' => ProductController::IMAGE_PATH . '/product_' . $this->faker->numberBetween(1, 32) . '.jpg',
        ]);
    }
}
