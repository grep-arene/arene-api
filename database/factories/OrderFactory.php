<?php

namespace Database\Factories;

use App\Helpers\PaymentStatus;
use App\Helpers\OrderStatus;
use App\Models\Order;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Order::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'date' => $this->faker->dateTime->format('Y-m-d H:i:s'),
            'payment' => PaymentStatus::getStatus()[rand(0, 2)],
            'discount' => $this->faker->randomFloat(2, 0, 99),
            'status' => OrderStatus::getStatus()[rand(0, 2)],
        ];
    }
}
