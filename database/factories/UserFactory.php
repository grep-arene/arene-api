<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'nickname' => $this->faker->unique()->word,
            'email' => $this->faker->unique()->safeEmail,
            'email_verified_at' => now(),
            'password' => bcrypt($this->faker->password),
            'remember_token' => Str::random(10),
            'is_admin' => false,
            'address' => $this->faker->address,
            'phone' => $this->faker->phoneNumber,
            'end_membership' => $this->faker->dateTimeThisYear('+100 days')->format('Y-m-d H:i:s'),
        ];
    }

    public function admin(): UserFactory
    {
        return $this->state(fn() => [
            'email' => 'admin@admin.com',
            'password' => bcrypt('admin@admin.com'),
            'is_admin' => true,
        ]);
    }
}
