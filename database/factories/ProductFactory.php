<?php

namespace Database\Factories;

use App\Http\Controllers\API\ProductController;
use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->word,
            'image_path' => null,
            'description' => $this->faker->text,
            'price' => $this->faker->randomFloat(2, 0, 99),
            'stock' => $this->faker->randomNumber(3),
            'minimum_stock' => $this->faker->randomNumber(3),
            'release_date' => $this->faker->date(),
            'is_orderable' => $this->faker->boolean(),
        ];
    }

    public function image(): ProductFactory
    {
        return $this->state(fn() => [
            'image_path' => ProductController::IMAGE_PATH . '/product_' . $this->faker->numberBetween(1, 32) . '.jpg',
        ]);
    }
}
