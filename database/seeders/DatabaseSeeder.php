<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            //seed pour remplir la base de données avec des données de tests génerés randomiquement
//            LanguageSeeder::class,
//            GameSeeder::class,
//            EventSeeder::class,
//            CategorySeeder::class,
//            ProductSeeder::class,
//            MembershipSeeder::class,
            UserSeeder::class,
//            OrderSeeder::class,
//            BuySeeder::class,
//            ParticipantSeeder::class,
//            PlayerSeeder::class,
//            InfoSeeder::class,
        ]);
    }
}
