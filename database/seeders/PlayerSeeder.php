<?php

namespace Database\Seeders;

use App\Models\Game;
use App\Models\Player;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class PlayerSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Player::factory()
            ->count(5)
            ->state(new Sequence(
                fn() => [
                    'user_id' => User::all()->random(),
                    'game_id' => Game::all()->random(),
                ]
            ))
            ->create();
    }
}
