<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Language;
use App\Models\Product;
use App\Models\Game;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::factory()
            ->image()
            ->count(100)
            ->state(new Sequence(
                fn() => [
                    'category_id' => Category::all()->random(),
                    'game_id' => Game::all()->random(),
                    'language_id' => Language::all()->random(),
                ]
            ))
            ->create();
    }
}
