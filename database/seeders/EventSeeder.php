<?php

namespace Database\Seeders;

use App\Models\Event;
use App\Models\Game;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Event::factory()
            ->image()
            ->count(100)
            ->state(new Sequence(
                fn() => ['game_id' => Game::all()->random(),]
            ))
            ->create();
    }
}
