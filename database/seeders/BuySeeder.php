<?php

namespace Database\Seeders;

use App\Models\Order;
use App\Models\Product;
use App\Models\Buy;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class BuySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Buy::factory()
            ->count(5)
            ->state(new Sequence(
                fn() => [
                    'order_id' => Order::all()->random(),
                    'product_id' => Product::all()->random(),
                ]
            ))
            ->create();
    }
}
