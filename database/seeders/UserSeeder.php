<?php

namespace Database\Seeders;

use App\Models\Membership;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory()
            ->admin()
            ->create();

        //utilisateurs pour developpement
//        User::factory()
//            ->count(50)
//            ->create();
//
//        User::factory()
//            ->count(50)
//            ->state(new Sequence(
//                fn() => [
//                    'membership_id' => Membership::all()->random(),
//                ]
//            ))
//            ->create();
    }
}
