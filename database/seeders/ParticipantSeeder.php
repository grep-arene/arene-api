<?php

namespace Database\Seeders;

use App\Models\Event;
use App\Models\User;
use App\Models\Participant;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class ParticipantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Participant::factory()
            ->count(5)
            ->state(new Sequence(
                fn() => [
                    'event_id' => Event::all()->random(),
                    'user_id' => User::all()->random(),
                ]
            ))
            ->create();
    }
}
