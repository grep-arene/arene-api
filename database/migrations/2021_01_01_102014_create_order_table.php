<?php

use App\Helpers\OrderStatus;
use App\Helpers\PaymentStatus;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->datetime('date')->useCurrent();
            $table->enum('payment', PaymentStatus::getStatus())->default(PaymentStatus::PENDING);
            $table->decimal('discount')->nullable();
            $table->enum('status', OrderStatus::getStatus())->default(OrderStatus::CREATED);
            $table->foreignId('user_id')->default(0)->constrained();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
