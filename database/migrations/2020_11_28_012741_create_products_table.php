<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('image_path')->nullable();
            $table->longText('description')->nullable();
            $table->decimal('price');
            $table->integer('stock')->nullable();
            $table->integer('minimum_stock')->nullable();
            $table->date('release_date')->nullable();
            $table->boolean('is_orderable')->default(true);
            $table->foreignId('category_id')->nullable()->constrained();
            $table->foreignId('game_id')->nullable()->constrained();
            $table->foreignId('language_id')->nullable()->constrained();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
