---PLEASE DO NOT REPLAY TO THIS E-MAIL !---
<br><br>
================================================================================
<br><br>
Bonjour,
<br><br>
Nous confirmons votre commande numéro {{$order->id}}.
<br><br>
Voici la liste des articles commandés :
<br><br>
@foreach ($order->products as $product)
  {{$product->pivot->quantity}}x  {{ $product->name }} à {{ $product->price}} CHF/unité pour un total de {{ $product->price*$product->pivot->quantity }} CHF. <br>
@endforeach
<br>
Le prix total de la commande est de {{$total}} CHF.
<br><br>

Nous vous remercions pour cet achat.
<br><br>
================================================================================<br><br>
{{env('SHOP_NAME')}}
<br>
{{env('SHOP_PHONE')}}
<br>
{{env('SHOP_ADDRESS')}}
<br>
