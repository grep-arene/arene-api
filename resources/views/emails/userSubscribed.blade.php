---PLEASE DO NOT REPLAY TO THIS E-MAIL !---
<br><br>
================================================================================
<br><br>
Bonjour,
<br><br>
Nous confirmons votre inscription à l'événement : {{$event->name}}.
<br><br>
Il aura lieu le {{$event->date}}.
<br><br>
Merci pour votre participation.
<br><br>
================================================================================<br><br>
{{env('SHOP_NAME')}}
<br>
{{env('SHOP_PHONE')}}
<br>
{{env('SHOP_ADDRESS')}}
<br>
