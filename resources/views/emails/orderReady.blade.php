---PLEASE DO NOT REPLAY TO THIS E-MAIL !---
<br><br>
================================================================================
<br><br>
Bonjour,
<br><br>
Nous vous informons que votre commande numéro {{$order->id}} est prête à être retirée en magasin.
<br><br>
À bientôt.
<br><br>
================================================================================<br><br>
{{env('SHOP_NAME')}}
<br>
{{env('SHOP_PHONE')}}
<br>
{{env('SHOP_ADDRESS')}}
<br>
