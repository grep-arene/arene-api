<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>L'Arène de Duel</title>
    <div class="alert" style="display:none">
        <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
        <span id="message"></span>
    </div>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #f1f1f1;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .alert {
            padding: 20px;
            background-color: #f44336;
            color: white;
        }

        .closebtn {
            margin-left: 15px;
            color: white;
            font-weight: bold;
            float: right;
            font-size: 22px;
            line-height: 20px;
            cursor: pointer;
            transition: 0.3s;
        }

        .closebtn:hover {
            color: black;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        input {
            padding: 10pt;
            width: 60%;
            font-size: 15pt;
            border-radius: 5pt;
            border: 1px solid lightgray;
            margin: 10pt;
        }

        .form-container {
            display: flex;
            flex-direction: column;
            width: 60%;
            align-items: center;
            margin: 20pt;
            border: 1px solid lightgray;
            padding: 20pt;
            border-radius: 5pt;
            background: white;
        }

        button {
            border-radius: 5pt;
            padding: 10pt 14pt;
            background: white;
            border: 1px solid gray;
            font-size: 14pt;
            margin: 20pt;
        }

        button:hover {
            background: lightgray;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">
    <form class="form-container" action="/api/reset-password" method="POST">
        @csrf
        <h1>L'Arène de Duel</h1>
        <h2>Réinitialisation du mot de passe</h2>
        <input name="email" placeholder="Votre adresse mail" value="{{$request->email}}" required>
        <input id="password" type="password" name="password" placeholder="Nouveau mot de passe" required>
        <input id="password_confirmation" type="password" name="password_confirmation" placeholder="Confirmation du mot de passe" required>
        <input type="hidden" name="token" value="{{ $request->route('token') }}">

        <button id="submit" style="cursor: pointer;" type="submit">Confirmer</button>
    </form>
</div>

<script
    src="https://code.jquery.com/jquery-3.6.0.slim.min.js"
    integrity="sha256-u7e5khyithlIdTpu22PHhENmPcRdFiHRjhAuHcs05RI="
    crossorigin="anonymous"></script>

<script>
    $("form").submit(function (e) {
        if ($("#password").val() !== $("#password_confirmation").val()) {
            e.preventDefault();
            $("#message").text("Les mots de passes ne sont pas identiques.");
            $(".alert").css("display", "block");
        }
    });
</script>
</body>
</html>
