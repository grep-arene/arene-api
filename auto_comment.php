<?php
$path = "app/Models";
$dir = new DirectoryIterator($path);
foreach ($dir as $fileinfo) {
    if (!$fileinfo->isDot()) {
        $filename = $fileinfo->getFilename();
        comment($path . "/" . $filename, 'use FilterQueryString;');
    }
}

function comment($path_to_filename, $text)
{
    $uncommented = file_get_contents($path_to_filename);
    $commented = str_replace($text, '//' . $text, $uncommented);
    file_put_contents($path_to_filename, $commented);
}
