<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/reset-password/{token}', function () {
    return view('auth.reset-password', ['request' => request()]);
})->name('password.reset');

Route::get('/reset-done', function () {
    return view('auth.reset-done');
})->name('reset.done');
