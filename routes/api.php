<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\CategoryController;
use App\Http\Controllers\API\ContactController;
use App\Http\Controllers\API\EventController;
use App\Http\Controllers\API\GameController;
use App\Http\Controllers\API\InfoController;
use App\Http\Controllers\API\LanguageController;
use App\Http\Controllers\API\MemberController;
use App\Http\Controllers\API\MembershipController;
use App\Http\Controllers\API\OrderController;
use App\Http\Controllers\API\ParticipantController;
use App\Http\Controllers\API\PlayerController;
use App\Http\Controllers\API\ProductController;
use App\Http\Controllers\API\UserController;
use App\Http\Middleware\EnsureUserIsAdmin;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/contact', [ContactController::class, 'index']);

Route::get('/categories', [CategoryController::class, 'index']);
Route::get('/categories/{id}', [CategoryController::class, 'show']);

Route::get('/events', [EventController::class, 'index']);
Route::get('/events/{id}', [EventController::class, 'show']);

Route::get('/games', [GameController::class, 'index']);
Route::get('/games/{id}', [GameController::class, 'show']);

Route::get('/info', [InfoController::class, 'index']);

Route::get('/languages', [LanguageController::class, 'index']);
Route::get('/languages/{id}', [LanguageController::class, 'show']);

Route::post('/login', [AuthController::class, 'login']);
Route::post('/login-backoffice', [AuthController::class, 'loginBackOffice']);
Route::post('/register', [AuthController::class, 'register']);

Route::get('/products-low-supply', [ProductController::class, 'indexLowSupply']);
Route::get('/products', [ProductController::class, 'index']);
Route::get('/products/{id}', [ProductController::class, 'show']);

Route::post('/forgot-password', [AuthController::class, 'forgotPassword']);

Route::post('/reset-password', [AuthController::class, 'resetPassword']);

Route::get('/memberships', [MembershipController::class, 'index']);
Route::get('/memberships/{id}', [MembershipController::class, 'show']);

Route::middleware('auth:sanctum')->group(function () {

    Route::get('/members/{id}', [MemberController::class, 'show']);

    Route::get('/orders/{id}', [OrderController::class, 'show']);
    Route::post('/buy', [OrderController::class, 'buy']);
    Route::post('/pay/{id}', [OrderController::class, 'pay']);

    Route::post('/players', [PlayerController::class, 'store']);

    Route::post('/participants', [ParticipantController::class, 'store']);
    Route::delete('/participants/{id}', [ParticipantController::class, 'destroy']);

    Route::get('/users/{id}', [UserController::class, 'show']);
    Route::get('/users/{id}/events', [UserController::class, 'showEvents']);
    Route::get('/users/{id}/games', [UserController::class, 'showGames']);
    Route::get('/users/{id}/orders', [UserController::class, 'showOrders']);
    Route::put('/users/{id}', [UserController::class, 'update']);
    Route::delete('/users/{id}', [UserController::class, 'destroy']);

    Route::get('/is-participant/{id}', [EventController::class, 'showIsParticipant']);

    Route::post('/logout', [AuthController::class, 'logout']);
    Route::get('/user', [AuthController::class, 'current']);


    Route::middleware([EnsureUserIsAdmin::class])->group(function () {

        Route::post('/categories', [CategoryController::class, 'store']);
        Route::put('/categories/{id}', [CategoryController::class, 'update']);
        Route::delete('/categories/{id}', [CategoryController::class, 'destroy']);

        Route::post('/events', [EventController::class, 'store']);
        Route::put('/events/{id}', [EventController::class, 'update']);
        Route::delete('/events/{id}', [EventController::class, 'destroy']);
        Route::get('/events/{id}/users', [EventController::class, 'showParticipants']);

        Route::get('/games/-/users/{id}', [GameController::class, 'indexWithPlayer']);
        Route::post('/games', [GameController::class, 'store']);
        Route::put('/games/{id}', [GameController::class, 'update']);
        Route::delete('/games/{id}', [GameController::class, 'destroy']);

        Route::put('/info', [InfoController::class, 'update']);

        Route::post('/languages', [LanguageController::class, 'store']);
        Route::put('/languages/{id}', [LanguageController::class, 'update']);
        Route::delete('/languages/{id}', [LanguageController::class, 'destroy']);

        Route::get('/members', [MemberController::class, 'index']);
        Route::put('/members/{id}', [MemberController::class, 'update']);
        Route::get('/non-members', [MemberController::class, 'nonMembersIndex']);

        Route::get('/orders', [OrderController::class, 'index']);
        Route::put('/orders/{id}', [OrderController::class, 'update']);
        Route::post('/orders', [OrderController::class, 'store']);
        Route::delete('/orders/{id}', [OrderController::class, 'destroy']);

        Route::put('/participants/{id}', [ParticipantController::class, 'update']);

        Route::post('/products', [ProductController::class, 'store']);
        Route::put('/products/{id}', [ProductController::class, 'update']);
        Route::delete('/products/{id}', [ProductController::class, 'destroy']);

        Route::post('/memberships', [MembershipController::class, 'store']);
        Route::put('/memberships/{id}', [MembershipController::class, 'update']);
        Route::delete('/memberships/{id}', [MembershipController::class, 'destroy']);

        Route::get('/users', [UserController::class, 'index']);
        Route::post('/users', [UserController::class, 'store']);
    });
});




