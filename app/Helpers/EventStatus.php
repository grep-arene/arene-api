<?php

namespace App\Helpers;

abstract class EventStatus
{
    const PAID = 'paid';
    const UNPAID = 'unpaid';
    const CANCELLED = 'cancelled';

    public static function getStatus(): array
    {
        return [self::PAID, self::UNPAID, self::CANCELLED,];
    }
}
