<?php


namespace App\Helpers;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class FileHelper
{
    public static function handleImage(Request $request, Model $model, string $directory): Model
    {
        return self::handleFile($request, $model, $directory, 'image');
    }

    public static function handleFile(Request $request, Model $model, string $directory, string $file_name): Model
    {
        if ($request->hasFile($file_name)) {
            $path = self::saveFile($request->file($file_name), $model, $directory);
            if ($path != $model[$file_name . '_path']) {
                Storage::disk('public')->delete($model[$file_name . '_path']);
                $model[$file_name . '_path'] = $path;
            }
        } else if ($model[$file_name . '_path'] && empty($request->input($file_name . '_path'))) {
            Storage::disk('public')->delete($model[$file_name . '_path']);
            $model[$file_name . '_path'] = null;
        }
        return $model;
    }

    public static function saveFile($file, Model $model, string $directory): string
    {
        $name = $model->getTable() . '_' . $model->id . '.' . $file->extension();
        return $file->storeAs($directory, $name, 'public');
    }
}
