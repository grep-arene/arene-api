<?php

namespace App\Helpers;

abstract class OrderStatus
{
    const CREATED = 'created';
    const READY = 'ready';
    const COMPLETED = 'completed';
    const CANCELLED = 'cancelled';

    public static function getStatus(): array
    {
        return [self::CREATED, self::READY, self::COMPLETED, self::CANCELLED,];
    }
}
