<?php

namespace App\Helpers;

abstract class PaymentStatus
{
    const PENDING = 'pending';
    const TO_BE_VERIFIED = 'to be verified';
    const PAID = 'paid';

    public static function getStatus(): array
    {
        return [self::PENDING, self::TO_BE_VERIFIED, self::PAID,];
    }
}
