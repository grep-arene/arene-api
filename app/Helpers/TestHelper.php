<?php


namespace App\Helpers;


use App\Models\User;
use Laravel\Sanctum\Sanctum;

class TestHelper
{
    static function logUser(User $user)
    {
        Sanctum::actingAs($user);
    }

    static function logClient()
    {
        Sanctum::actingAs(User::factory()->create());
    }

    static function logAdmin()
    {
        Sanctum::actingAs(User::factory()->create(['is_admin' => true]));
    }
}
