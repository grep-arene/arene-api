<?php


namespace App\Helpers;


use Illuminate\Http\Request;

class FilterHelper
{
    public static function convertToLike(Request $request, array|string $key): Request
    {
        if (is_array($key)) {
            return self::convertManyToLike($request, $key);
        }
        if ($request->query($key)) {
            $request->merge(['like' => $key . ',' . $request->query($key)]);
            $request->query->remove($key);
        }
        return $request;
    }

    public static function convertManyToLike(Request $request, array $keys): Request
    {
        $likes = [];
        foreach ($keys as $key) {
            if ($request->query($key)) {
                array_push($likes, $key . ',' . $request->query($key));
                $request->query->remove($key);
            }
        }
        $request->merge(['like' => $likes]);
        return $request;
    }

}
