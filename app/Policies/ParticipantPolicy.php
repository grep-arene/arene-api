<?php /** @noinspection PhpMissingReturnTypeInspection @noinspection PhpInconsistentReturnPointsInspection */

namespace App\Policies;

use App\Models\Participant;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ParticipantPolicy
{
    use HandlesAuthorization;

    /**
     * Perform pre-authorization checks.
     *
     * @param User $user
     * @return bool
     */
    public function before(User $user)
    {
        if ($user->is_admin) return true;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @param Participant $participant
     * @return bool
     */
    public function view(User $user, Participant $participant): bool
    {
        return $user->id === $participant->id;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param User $user
     * @param Participant $participant
     * @return bool
     */
    public function update(User $user, Participant $participant): bool
    {
        return $user->id === $participant->id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param User $user
     * @param Participant $participant
     * @return bool
     */
    public function delete(User $user, Participant $participant): bool
    {
        return $user->id === $participant->id;
    }
}
