<?php

namespace App\Http\Controllers\API;

use App\Mail\ContactMail;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Mail;
use App\Http\Controllers\Controller;


class ContactController extends Controller
{
    public function index(Request $request): Response
    {
        try {
            $request->input('subject');
            Mail::to(env('MAIL_USERNAME'))->send(new ContactMail($request));
            return response("mail envoyé");
        } catch (Exception) {
            return response("failed to send");
        }
    }

}
