<?php

namespace App\Http\Controllers\API;

use App\Exceptions\Message;
use App\Helpers\FileHelper;
use App\Helpers\FilterHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Models\Product;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    const IMAGE_PATH = 'product-images';

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request): Response
    {
        FilterHelper::convertToLike($request, 'name');
        $products = Product::filter()
            ->with(['category', 'game', 'language'])
            ->orderBy('updated_at', 'desc')
            ->paginate(20);
        return response($products);
    }

    /**
     * Display a listing of the products with low supply.
     *
     * @param Request $request
     * @return Response
     */
    public function indexLowSupply(Request $request): Response
    {
        FilterHelper::convertToLike($request, 'name');
        $products = Product::whereColumn('stock', '<', 'minimum_stock')
            ->with(['category', 'game', 'language'])
            ->orderBy('updated_at', 'desc')
            ->paginate(20);

        return response($products);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreProductRequest $request
     * @return Response
     */
    public function store(StoreProductRequest $request): Response
    {
        $product = new Product;
        $product->fill($request->validated());

        if ($request->hasFile('image')) {
            $product->image_path = FileHelper::saveFile($request->file('image'), $product, self::IMAGE_PATH);
        }
        $product->save();
        $product->refresh()->load('category', 'game', 'language');
        return response($product, Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show(int $id): Response
    {
        try {
            $product = Product::findOrFail($id);
            $product->load('category', 'game', 'language');
            return response($product);
        } catch (ModelNotFoundException) {
            return response(Message::FAILED_VIEW, Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateProductRequest $request
     * @param int $id
     * @return Response
     */
    public function update(UpdateProductRequest $request, int $id): Response
    {
        try {
            $product = Product::findOrFail($id);
            FileHelper::handleImage($request, $product, self::IMAGE_PATH);

            $product->fill($request->validated());
            $product->save();
            $product->refresh()->load('category', 'game', 'language');
            return response($product);
        } catch (ModelNotFoundException) {
            return response(Message::FAILED_UPDATE, Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy(int $id): Response
    {
        try {
            $product = Product::findOrFail($id);
            if ($product->image_path) {
                Storage::disk('public')->delete($product->image_path);
            }
            $product->delete();
            return response(null);
        } catch (ModelNotFoundException) {
            return response(Message::FAILED_DELETED, Response::HTTP_NOT_FOUND);
        } catch (Exception $e) {
            return response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
