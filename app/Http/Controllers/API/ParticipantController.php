<?php

namespace App\Http\Controllers\API;

use App\Exceptions\AlreadySubscribedException;
use App\Exceptions\Message;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateParticipantRequest;

use App\Mail\UserSubscribed;
use App\Models\Event;
use Exception;
use App\Models\Participant;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;

class ParticipantController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request): Response
    {
        try {
            $event = Event::findOrFail($request->id);
            $user = $request->user();
            $event->addParticipant($user);
            if (App::environment('testing'))
                echo 'Testing environment. the mail has not been delivered';
            else {
                Mail::to($request->user())->send(new UserSubscribed($event));
            }
            return response(Message::SUCCESSFUL_SUBSCRIPTION, Response::HTTP_OK);
        } catch (ModelNotFoundException) {
            return response(Message::FAILED_UPDATE, Response::HTTP_NOT_FOUND);
        } catch (AlreadySubscribedException) {
            return response(Message::FAILED_SUBSCRIPTION, Response::HTTP_CONFLICT);
        }
    }

    public function update(UpdateParticipantRequest $request, int $id): Response
    {
        try {
            $participant = Participant::findOrFail($id);
            $this->authorize('update', $participant);
            $participant->fill($request->validated())->save();
            return response($participant);
        } catch (AuthorizationException) {
            return response(Message::FORBIDDEN_USER, Response::HTTP_FORBIDDEN);
        } catch (ModelNotFoundException) {
            return response(Message::FAILED_DELETED, Response::HTTP_NOT_FOUND);
        } catch (Exception $e) {
            return response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function destroy(int $id): Response
    {
        try {
            $participant = Participant::findOrFail($id);
            $this->authorize('delete', $participant);
            $participant->delete();
            return response(null);
        } catch (AuthorizationException) {
            return response(Message::FORBIDDEN_USER, Response::HTTP_FORBIDDEN);
        } catch (ModelNotFoundException) {
            return response(Message::FAILED_DELETED, Response::HTTP_NOT_FOUND);
        } catch (Exception $e) {
            return response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
