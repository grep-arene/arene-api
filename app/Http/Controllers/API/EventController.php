<?php

namespace App\Http\Controllers\API;

use App\Exceptions\Message;
use App\Helpers\FileHelper;
use App\Helpers\FilterHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreEventRequest;
use App\Http\Requests\UpdateEventRequest;
use App\Models\Event;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class EventController extends Controller
{
    const RESULT_PATH = 'event-results';
    const IMAGE_PATH = 'event-images';

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request): Response
    {
        $request = FilterHelper::convertToLike($request, 'name');
        if ($request->query('page')) {
            $events = Event::filter()
                ->with(['game'])
                ->orderBy('updated_at', 'desc')
                ->paginate(20);
        } else {
            $events = Event::filter()
                ->with(['game'])
                ->orderBy('updated_at', 'desc')
                ->get();
        }
        return response($events);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreEventRequest $request
     * @return Response
     */
    public function store(StoreEventRequest $request): Response
    {
        $event = new Event;
        if ($request->hasFile('result')) {
            $event->result_path = FileHelper::saveFile($request->file('result'), $event, self::RESULT_PATH);
        }
        if ($request->hasFile('image')) {
            $event->image_path = FileHelper::saveFile($request->file('image'), $event, self::IMAGE_PATH);
        }
        $event->fill($request->validated());
        $event->save();
        $event->refresh()->load('game');
        return response($event, Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show(int $id): Response
    {
        try {
            $event = Event::findOrFail($id);
            $event->getAvailablePlaces();
            $event->load('game');
            return response($event);
        } catch (ModelNotFoundException) {
            return response(Message::FAILED_VIEW, Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function showIsParticipant(Request $request, int $id): Response
    {
        try {
            $event = Event::findOrFail($id);
            $event->getAvailablePlaces();
            $event->load('game');
            $request->user();
            $event->is_participant = $event->is_participant($request->user());
            return response($event);
        } catch (ModelNotFoundException) {
            return response(Message::FAILED_VIEW, Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateEventRequest $request
     * @param int $id
     * @return Response
     */
    public function update(UpdateEventRequest $request, int $id): Response
    {
        try {
            $event = Event::findOrFail($id);
            FileHelper::handleFile($request, $event, self::RESULT_PATH, 'result');
            FileHelper::handleImage($request, $event, self::IMAGE_PATH);

            $users = $request->input('users');
            if ($users) {
                $event->users()->detach();
                $event->users()->attach($users);
            }

            $event->fill($request->validated());
            $event->save();
            $event->refresh()->load('game');
            return response($event);
        } catch (ModelNotFoundException) {
            return response(Message::FAILED_UPDATE, Response::HTTP_NOT_FOUND);
        }
    }

    public function showParticipants(int $id): Response
    {
        try {
            $event = Event::findOrFail($id);
            return response($event->users);
        } catch (ModelNotFoundException) {
            return response(Message::FAILED_VIEW, Response::HTTP_NOT_FOUND);
        } catch (Exception $e) {
            return response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy(int $id): Response
    {
        try {
            $event = Event::findOrFail($id);
            if ($event->result_path) {
                Storage::disk('public')->delete($event->result_path);
            }
            if ($event->image_path) {
                Storage::disk('public')->delete($event->image_path);
            }
            $event->users()->delete();
            $event->delete();
            return response(null);
        } catch (ModelNotFoundException) {
            return response(Message::FAILED_DELETED, Response::HTTP_NOT_FOUND);
        } catch (Exception $e) {
            return response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
