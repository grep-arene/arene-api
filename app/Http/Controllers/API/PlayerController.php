<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PlayerController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request): Response
    {
        $user = $request->user();
        $user->games()->detach();
        $user->games()->attach($request->all());

        return response($user->games, Response::HTTP_CREATED);
    }
}
