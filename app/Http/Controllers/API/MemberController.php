<?php

namespace App\Http\Controllers\API;

use App\Exceptions\Message;
use App\Helpers\FilterHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateMemberRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request): Response
    {
        $request = FilterHelper::convertToLike($request, ['email', 'name']);
        $members = $request->query('onlyActive') == 'true'
            ? User::activeMembers()->orderBy('updated_at', 'desc')->paginate(20)
            : User::members()->orderBy('updated_at', 'desc')->paginate(20);
        return response($members);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function nonMembersIndex(): Response
    {
        $members = User::nonMembers()
            ->orderBy('updated_at', 'desc')
            ->paginate(20);
        return response($members);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateMemberRequest $request
     * @param int $id
     * @return Response
     */
    public function update(UpdateMemberRequest $request, int $id): Response
    {
        try {
            $user = User::findOrFail($id);
            $user->fill($request->validated())->save();
            $user->refresh()->load('membership');
            return response($user);
        } catch (ModelNotFoundException) {
            return response(Message::FAILED_UPDATE, Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show(int $id): Response
    {
        try {
            $user = User::where('membership_id', '>', 0)
                ->findOrFail($id);
            $this->authorize('view', $user);
            return response($user);
        } catch (AuthorizationException) {
            return response(Message::FORBIDDEN_USER, Response::HTTP_FORBIDDEN);
        } catch (ModelNotFoundException) {
            return response(Message::FAILED_VIEW, Response::HTTP_NOT_FOUND);
        }
    }
}
