<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreInfoRequest;
use App\Models\Info;
use Illuminate\Http\Response;

class InfoController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @return Response
     */
    public function index(): Response
    {
        $info = Info::first();
        return response($info);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreInfoRequest $request
     * @return Response
     */
    public function update(StoreInfoRequest $request): Response
    {
        $info = Info::first();
        $info->fill($request->validated())->save();
        return response($info);
    }
}
