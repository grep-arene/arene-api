<?php

namespace App\Http\Controllers\API;

use App\Exceptions\Message;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\StoreClientRequest;
use App\Models\User;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function current(Request $request): Response
    {
        $user = $request->user();
        $user->password = '';
        return response($user);
    }

    public function register(StoreClientRequest $request): Response
    {
        $validated = $request->validated();
        $validated['password'] = bcrypt($validated['password']);
        $user = new User;
        $user->fill($validated)->save();
        $user = $user->refresh();

        $result = [
            'access_token' => self::createToken($user),
            'user' => $user->toArray(),
        ];
        return response($result, Response::HTTP_CREATED);
    }

    public function login(LoginRequest $request): Response
    {
        $user = User::whereEmail($request->email)->first();

        if (!$user || !Hash::check($request->password, $user->password)) {
            return response(Message::FAILED_LOGIN, Response::HTTP_UNAUTHORIZED);
        }

        $result = [
            'access_token' => self::createToken($user),
            'user' => $user->toArray(),
        ];
        return response($result);
    }

    public function loginBackOffice(LoginRequest $request): Response
    {
        $user = User::whereEmail($request->email)->first();

        if (!$user || $user->is_admin == false || !Hash::check($request->password, $user->password)) {
            return response(Message::FAILED_LOGIN, Response::HTTP_UNAUTHORIZED);
        }

        $result = [
            'access_token' => self::createToken($user),
            'user' => $user->toArray(),
        ];
        return response($result);
    }

    public static function createToken(User $user): string
    {
        return $user->createToken('authToken')->plainTextToken;
    }

    public function logout(Request $request): Response
    {
        $request->user()->currentAccessToken()->delete();
        return response(Message::SUCCESSFUL_LOGOUT);
    }

    public function forgotPassword(Request $request): Response
    {
        $request->validate(['email' => 'required|email']);

        $status = Password::sendResetLink($request->only('email'));

        $status === Password::RESET_LINK_SENT
            ? back()->with(['status' => __($status)])
            : back()->withErrors(['email' => __($status)]);

        return response(['msg' => $status]);
    }

    public function resetPassword(Request $request): RedirectResponse
    {
        $request->validate([
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed',
        ]);

        $status = Password::reset(
            $request->only('email', 'password', 'password_confirmation', 'token'),
            function ($user, $password) use ($request) {
                $user->forceFill([
                    'password' => Hash::make($password)
                ])->setRememberToken(Str::random(60));

                $user->save();

                event(new PasswordReset($user));
            }
        );

        return $status == Password::PASSWORD_RESET
            ? redirect('/reset-done')->with('status', __($status))
            : back()->withInput($request->only('email'))
                ->withErrors(['email' => __($status)]);
    }
}
