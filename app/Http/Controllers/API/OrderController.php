<?php

namespace App\Http\Controllers\API;

use App\Helpers\OrderStatus;
use App\Helpers\PaymentStatus;
use App\Http\Controllers\Controller;
use App\Exceptions\Message;
use App\Http\Requests\StoreOrderRequest;
use App\Http\Requests\UpdateOrderRequest;

use App\Mail\OrderMail;
use App\Mail\OrderReady;
use App\Models\Order;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(): Response
    {
        $orders = Order::filter()
            ->with(['products', 'user'])
            ->orderBy('updated_at', 'desc')
            ->paginate(20);
        return response($orders);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreOrderRequest $request
     * @return Response
     */
    public function store(StoreOrderRequest $request): Response
    {
        $order = new Order;
        $order->fill($request->validated())->save();

        $products = $this::convertToAttach($request->products);
        $order->products()->attach($products);
        if ($order->status != OrderStatus::CANCELLED) {
            self::decreaseStocks($order->products);
        }
        $order->refresh()->load('products', 'user');

        return response($order, Response::HTTP_CREATED);
    }

    private static function convertToAttach($items): array
    {
        $new_items = [];
        foreach ($items as $item) {
            $new_items[$item['id']] = $item['pivot'];
        }
        return $new_items;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function buy(Request $request): Response
    {
        $buyer = $request->user();
        $order = Order::create(['user_id' => $buyer->id]);

        $products = $this::convertToAttach($request->all());
        $order->products()->attach($products);
        self::decreaseStocks($order->products);
        $order->refresh()->load('products', 'user');

        if (App::environment('testing'))
            echo 'Testing environment. the mail has not been delivered';
        else
            Mail::to($buyer)->send(new OrderMail($order));
        return response($order, Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show(int $id): Response
    {
        try {
            $order = Order::findOrFail($id);
            $this->authorize('view', $order);
            $order->load('products', 'user');
            return response($order);
        } catch (AuthorizationException) {
            return response(Message::FORBIDDEN_USER, Response::HTTP_FORBIDDEN);
        } catch (ModelNotFoundException) {
            return response(Message::FAILED_VIEW, Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     * @return Response
     */
    public function pay(int $id): Response
    {
        try {
            $order = Order::findOrFail($id);
            $this->authorize('update', $order);
            $order->payment = PaymentStatus::TO_BE_VERIFIED;
            $order->save();
            $order->refresh()->load('products', 'user');
            return response($order);
        } catch (AuthorizationException) {
            return response(Message::FORBIDDEN_USER, Response::HTTP_FORBIDDEN);
        } catch (ModelNotFoundException) {
            return response(Message::FAILED_UPDATE, Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateOrderRequest $request
     * @param int $id
     * @return Response
     */
    public function update(UpdateOrderRequest $request, int $id): Response
    {
        try {
            $order = Order::findOrFail($id);
            $validated = $request->validated();

            if ($order->is_not_cancelled() && $request->status === OrderStatus::CANCELLED) {
                self::increaseStocks($order->products);
            }
            if ($order->is_cancelled() && $request->status !== OrderStatus::CANCELLED) {
                self::decreaseStocks($order->products);
            }
            if ($order->is_not_cancelled() && $request->status !== OrderStatus::CANCELLED) {
                self::increaseStocks($order->products);
                $order->products()->detach();
                $products = $this::convertToAttach($request->products);
                $order->products()->attach($products);
                $order->refresh();
                self::decreaseStocks($order->products);
            }

            if ($request->status === OrderStatus::READY && $order->status !== $request->status) {
                if (App::environment('testing'))
                    echo 'Testing environment. the mail has not been delivered';
                else
                    Mail::to($order->user->email)->send(new OrderReady($order));
            }

            $order->fill($validated)->save();
            $order->refresh()->load('products', 'user');
            return response($order);
        } catch (ModelNotFoundException) {
            return response(Message::FAILED_UPDATE, Response::HTTP_NOT_FOUND);
        }
    }

    private function decreaseStocks($products)
    {
        foreach ($products as $product) {
            $product->stock -= $product->pivot->quantity;
            $product->save();
        }
    }

    private function increaseStocks($products)
    {
        foreach ($products as $product) {
            $product->stock += $product->pivot->quantity;
            $product->save();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy(int $id): Response
    {
        try {
            $order = Order::findOrFail($id);
            if ($order->is_not_cancelled()) {
                self::increaseStocks($order->products);
            }
            $order->products()->detach();
            $order->delete();
            return response(null);
        } catch (ModelNotFoundException) {
            return response(Message::FAILED_DELETED, Response::HTTP_NOT_FOUND);
        } catch (Exception $e) {
            return response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
