<?php

namespace App\Http\Controllers\API;

use App\Exceptions\Message;
use App\Http\Controllers\Controller;
use App\Http\Requests\SaveCategoryRequest;
use App\Models\Category;
use Illuminate\Http\Response;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(): Response
    {
        return response(Category::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SaveCategoryRequest $request
     * @return Response
     */
    public function store(SaveCategoryRequest $request): Response
    {
        $category = new Category;
        $category->fill($request->validated())->save();
        return response($category, Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show(int $id): Response
    {
        try {
            $category = Category::findOrFail($id);
            return response($category);
        } catch (ModelNotFoundException) {
            return response(Message::FAILED_VIEW, Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SaveCategoryRequest $request
     * @param int $id
     * @return Response
     */
    public function update(SaveCategoryRequest $request, int $id): Response
    {
        try {
            $category = Category::findOrFail($id);
            $category->fill($request->validated())->save();
            return response($category);
        } catch (ModelNotFoundException) {
            return response(Message::FAILED_UPDATE, Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy(int $id): Response
    {
        try {
            $category = Category::findOrFail($id);
            $category->delete();
            return response(null);
        } catch (ModelNotFoundException) {
            return response(Message::FAILED_DELETED, Response::HTTP_NOT_FOUND);
        } catch (Exception $e) {
            return response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
