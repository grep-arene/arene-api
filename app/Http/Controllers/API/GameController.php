<?php

namespace App\Http\Controllers\API;

use App\Exceptions\Message;
use App\Helpers\FileHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\SaveGameRequest;
use App\Models\Game;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Response;

class GameController extends Controller
{
    const IMAGE_PATH = 'game-images';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(): Response
    {
        return response(Game::all());
    }

    /**
     * Display a listing of the resource with a certain player.
     *
     * @param int $userId
     * @return Response
     */
    public function indexWithPlayer(int $userId): Response
    {
        $games = Game::with(['players' =>
            fn($query) => $query->whereUserId($userId)])->get();
        return response($games);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SaveGameRequest $request
     * @return Response
     */
    public function store(SaveGameRequest $request): Response
    {
        $game = new Game();
        $game->fill($request->validated());

        if ($request->hasFile('image')) {
            $game->image_path = FileHelper::saveFile($request->file('image'), $game, self::IMAGE_PATH);
        }
        $game->save();
        return response($game, Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show(int $id): Response
    {
        try {
            $game = Game::findOrFail($id);
            return response($game);
        } catch (ModelNotFoundException) {
            return response(Message::FAILED_VIEW, Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SaveGameRequest $request
     * @param int $id
     * @return Response
     */
    public function update(SaveGameRequest $request, int $id): Response
    {
        try {
            $game = Game::findOrFail($id);
            FileHelper::handleImage($request, $game, self::IMAGE_PATH);

            $game->fill($request->validated());
            $game->save();
            return response($game);
        } catch (ModelNotFoundException) {
            return response(Message::FAILED_UPDATE, Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy(int $id): Response
    {
        try {
            $game = Game::findOrFail($id);
            $game->delete();
            return response(null);
        } catch (ModelNotFoundException) {
            return response(Message::FAILED_DELETED, Response::HTTP_NOT_FOUND);
        } catch (Exception $e) {
            return response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
