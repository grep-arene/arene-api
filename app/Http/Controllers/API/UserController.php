<?php

namespace App\Http\Controllers\API;

use App\Exceptions\Message;
use App\Helpers\FilterHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request): Response
    {
        FilterHelper::convertToLike($request, ['email', 'name']);
        $users = User::filter()
            ->orderBy('updated_at', 'desc')
            ->paginate(20);
        return response($users);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show(int $id): Response
    {
        try {
            $user = User::findOrFail($id);
            $this->authorize('view', $user);
            $user->load('membership');
            return response($user);
        } catch (AuthorizationException) {
            return response(Message::FORBIDDEN_USER, Response::HTTP_FORBIDDEN);
        } catch (ModelNotFoundException) {
            return response(Message::FAILED_VIEW, Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Display a listing of games from a user.
     *
     * @param int $id
     * @return Response
     */
    public function showGames(int $id): Response
    {
        try {
            $user = User::findOrFail($id);
            $this->authorize('view', $user);
            return response($user->games);
        } catch (AuthorizationException) {
            return response(Message::FORBIDDEN_USER, Response::HTTP_FORBIDDEN);
        } catch (ModelNotFoundException) {
            return response(Message::FAILED_VIEW, Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Display a listing of events from a user.
     *
     * @param int $id
     * @return Response
     */
    public function showEvents(int $id): Response
    {
        try {
            $user = User::findOrFail($id);
            $this->authorize('view', $user);
            return response($user->events);
        } catch (AuthorizationException) {
            return response(Message::FORBIDDEN_USER, Response::HTTP_FORBIDDEN);
        } catch (ModelNotFoundException) {
            return response(Message::FAILED_VIEW, Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Display a listing of orders from a user.
     *
     * @param int $id
     * @return Response
     */
    public function showOrders(int $id): Response
    {
        try {
            $user = User::findOrFail($id);
            $this->authorize('view', $user);
            return response($user->orders);
        } catch (AuthorizationException) {
            return response(Message::FORBIDDEN_USER, Response::HTTP_FORBIDDEN);
        } catch (ModelNotFoundException) {
            return response(Message::FAILED_VIEW, Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreUserRequest $request
     * @return Response
     */
    public function store(StoreUserRequest $request): Response
    {
        $validated = $request->validated();
        $validated['password'] = bcrypt($validated['password']);
        $user = new User;
        $user->fill($validated)->save();
        $user->refresh();
        return response($user, Response::HTTP_CREATED);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateUserRequest $request
     * @param int $id
     * @return Response
     */
    public function update(UpdateUserRequest $request, int $id): Response
    {
        try {
            $user = User::findOrFail($id);
            $this->authorize('update', $user);

            $validated = $request->validated();
            $user->fill(self::hashPassword($validated))->save();

            if (isset($validated['games']))
                $this->refreshPlayersId($user, $validated['games']);

            if (isset($validated['events']))
                $this->refreshEventStatuses($user, $validated['events']);

            return response($user);
        } catch (AuthorizationException) {
            return response(Message::FORBIDDEN_USER, Response::HTTP_FORBIDDEN);
        } catch (ModelNotFoundException) {
            return response(Message::FAILED_UPDATE, Response::HTTP_NOT_FOUND);
        }
    }

    private static function hashPassword($validated)
    {
        if (empty($validated['password'])) {
            unset($validated['password']);
        } else {
            $validated['password'] = bcrypt($validated['password']);
        }
        return $validated;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy(int $id): Response
    {
        try {
            $user = User::findOrFail($id);
            $this->authorize('delete', $user);
            $user->events()->detach();
            $user->games()->detach();
            $user->delete();
            return response(null);
        } catch (AuthorizationException) {
            return response(Message::FORBIDDEN_USER, Response::HTTP_FORBIDDEN);
        } catch (ModelNotFoundException) {
            return response(Message::FAILED_DELETED, Response::HTTP_NOT_FOUND);
        } catch (Exception $e) {
            return response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param User $user
     * @param $games
     * @return mixed
     */
    public function refreshPlayersId(User $user, $games): void
    {
        $user->games()->detach();
        $user->games()->attach($games);
    }

    /**
     * @param User $user
     * @param $events
     * @return mixed
     */
    public function refreshEventStatuses(User $user, $events): void
    {
        $user->events()->detach();
        $user->events()->attach($events);
    }
}
