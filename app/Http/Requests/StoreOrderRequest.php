<?php

namespace App\Http\Requests;

use App\Helpers\OrderStatus;
use App\Helpers\PaymentStatus;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => 'required|date',
            'payment' => Rule::in(PaymentStatus::getStatus()),
            'status' => Rule::in(OrderStatus::getStatus()),
            'user_id' => 'exists:users,id',
            'products' => 'required|array',
        ];
    }
}
