<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => 'nullable|image',
            'name' => 'nullable',
            'description' => 'nullable',
            'price' => 'nullable|numeric',
            'stock' => 'nullable|integer',
            'minimum_stock' => 'nullable|integer',
            'release_date' => 'nullable|date',
            'is_orderable' => 'nullable|boolean',
            'category_id' => 'exists:categories,id',
            'game_id' => 'exists:games,id',
            'language_id' => 'exists:languages,id',
        ];
    }
}
