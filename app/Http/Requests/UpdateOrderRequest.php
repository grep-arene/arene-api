<?php

namespace App\Http\Requests;

use App\Helpers\OrderStatus;
use App\Helpers\PaymentStatus;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'user_id' => 'exists:users,id',
            'discount' => 'nullable|numeric',
            'products' => 'nullable|array',
            'date' => 'nullable|date',
            'payment' => ['nullable', Rule::in(PaymentStatus::getStatus())],
            'status' => ['nullable', Rule::in(OrderStatus::getStatus())],
        ];
    }
}
