<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $user_id = $this->route('id');
        $rules = [
            'name' => 'nullable|string',
            'nickname' => [
                'nullable',
                Rule::unique('users')->ignore($user_id),
            ],
            'email' => [
                'nullable',
                Rule::unique('users')->ignore($user_id),
            ],
            'password' => 'nullable|string',
            'address' => 'nullable|string',
            'phone' => 'nullable',
            'games' => 'nullable|array',
        ];
        if ($this->user()->is_admin) {
            $extra = [
                'events' => 'nullable|array',
                'is_admin' => 'nullable|boolean',
            ];
            return array_merge($rules, $extra);
        }
        return $rules;
    }
}
