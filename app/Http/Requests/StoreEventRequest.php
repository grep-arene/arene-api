<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreEventRequest extends FormRequest
{
    /**
     *Determine if the event is authorized to make this request
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules(): array
    {
        return [
            'image' => 'nullable|image',
            'result' => 'nullable|file',
            'name' => 'required',
            'description' => 'nullable',
            'date' => 'required|date',
            'price' => 'required|numeric',
            'nb_place' => 'required|numeric',
            'game_id' => 'exists:games,id',
        ];
    }

}
