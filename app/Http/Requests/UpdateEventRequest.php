<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateEventRequest extends FormRequest
{
    /**
     *Determine if the event is authorized to make this request
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'image' => 'nullable|image',
            'result' => 'nullable|file',
            'name' => 'nullable',
            'description' => 'nullable',
            'date' => 'nullable',
            'price' => 'nullable|numeric',
            'nb_place' => 'nullable|numeric',
            'game_id' => 'exists:games,id',
            'users' => 'nullable|array',
        ];
    }

}
