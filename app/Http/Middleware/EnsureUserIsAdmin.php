<?php

namespace App\Http\Middleware;

use App\Exceptions\Message;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class EnsureUserIsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next): mixed
    {
        if ($request->user()->is_admin)
            return $next($request);
        else
            return response(Message::FORBIDDEN_USER, Response::HTTP_FORBIDDEN);
    }
}
