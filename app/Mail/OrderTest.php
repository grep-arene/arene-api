<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderTest extends Mailable
{
    use Queueable, SerializesModels;
    public $user, $order_id, $buys;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(String $user, int $order_id, array $buys )
    {
        $this->user =$user;
        $this->order_id = $order_id;
        $this->buys = $buys;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.orderTest')->subject("Votre commande à l'Arène de Duel");
    }
}
