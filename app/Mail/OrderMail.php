<?php

namespace App\Mail;

use App\Models\Buy;
use App\Models\Order;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Ramsey\Uuid\Type\Integer;

class OrderMail extends Mailable
{
    use Queueable, SerializesModels;


    public Order $order;
    public float $total = 0;

    /**
     * Create a new message instance.
     *
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        //
        $this->order = $order;
        foreach ($order->buys as $buy)
            $this->total += $buy->product->price*$buy->quantity;
        $this->total = $this->total-$order->discount;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.order')->subject("Votre commande à l'Arène de Duel");
    }
}
