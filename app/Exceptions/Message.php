<?php

namespace App\Exceptions;

class Message
{
    const FAILED_UPDATE = ['message' => 'The resource you are trying to update does not exist'];
    const FAILED_DELETED = ['message' => 'The resource you are trying to delete does not exist'];
    const FAILED_VIEW = ['message' => 'The resource you are trying to access does not exist'];
    const FORBIDDEN_USER = ['message' => 'You do not have permission to access this ressource'];
    const FAILED_LOGIN = ['message' => 'Incorrect username or password'];
    const SUCCESSFUL_LOGOUT = ['message' => 'Logout Successful'];
    const SUCCESSFUL_SUBSCRIPTION = ['message' => 'You have been successfully subscribed to this event'];
    const FAILED_SUBSCRIPTION = ['message' => 'You\'re already subscribed to this event, Can\'t subscribe twice'];
    const SUCCESSFUL_REGISTER = ['message' => 'User successfully created'];
    const UNAUTHENTICATED = ['message' => 'Unauthenticated.'];
}
