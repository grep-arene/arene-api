<?php /** @noinspection PhpUnused */

namespace App\Models;

use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use Laravel\Sanctum\HasApiTokens;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Mehradsadeghi\FilterQueryString\FilterQueryString;
use Illuminate\Auth\Passwords\CanResetPassword as CanResetPasswordTrait;

/**
 * @method static filter()
 * @mixin IdeHelperUser
 */
class User extends Authenticatable implements CanResetPassword
{
    use HasApiTokens, SoftDeletes, HasFactory, Notifiable, CanResetPasswordTrait;
    use FilterQueryString;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'nickname',
        'email',
        'password',
        'is_admin',
        'address',
        'phone',
        'end_membership',
        'membership_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'email_verified_at',
        'remember_token',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'membership_id' => 'integer',
        'is_admin' => 'boolean',
        'end_membership' => 'datetime:Y-m-d\TH:i:sO',
    ];

    protected array $filters = [
        'name',
        'nickname',
        'email',
        'is_admin',
        'address',
        'phone',
        'greater',
        'greater_or_equal',
        'less',
        'less_or_equal',
        'between',
        'not_between',
        'sort',
        'like',
    ];

    public function setEndMembershipAttribute($value)
    {
        $this->attributes['end_membership'] = date('Y-m-d H:i:s', strtotime($value));
    }

    public static function members()
    {
        return self::filter()
            ->with('membership')
            ->whereNotNull('membership_id');
    }

    public static function nonMembers()
    {
        return self::filter()
            ->whereNull('membership_id');
    }

    public static function activeMembers()
    {
        $today = Carbon::today();
        return self::filter()
            ->with('membership')
            ->whereNotNull('membership_id')
            ->whereDate('end_membership', '>', $today);
    }

    public function orders(): HasMany
    {
        return $this->hasMany(Order::class)->withTrashed();
    }

    public function games(): BelongsToMany
    {
        return $this->belongsToMany(Game::class, 'player')
            ->using(Player::class)
            ->withPivot('player_id')
            ->whereNull('player.deleted_at');
    }

    public function players(): HasMany
    {
        return $this->hasMany(Player::class)->withTrashed();
    }

    public function events(): BelongsToMany
    {
        return $this->belongsToMany(Event::class, 'participant')
            ->using(Participant::class)
            ->withPivot('status')
            ->whereNull('participant.deleted_at');
    }

    public function participants(): HasMany
    {
        return $this->hasMany(Participant::class)->withTrashed();
    }

    public function membership(): BelongsTo
    {
        return $this->belongsTo(Membership::class)->withTrashed();
    }
}
