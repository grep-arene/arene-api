<?php /** @noinspection PhpUndefinedMethodInspection */

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mehradsadeghi\FilterQueryString\FilterQueryString;

/**
 * @method static filter()
 * @mixin IdeHelperProduct
 */
class Product extends Model
{
    use HasFactory, SoftDeletes;
    use FilterQueryString;

    protected $fillable = [
        'name',
        'description',
        'price',
        'stock',
        'minimum_stock',
        'release_date',
        'is_orderable',
        'category_id',
        'game_id',
        'language_id',
    ];

    protected $casts = [
        'price' => 'double',
        'stock' => 'integer',
        'minimum_stock' => 'integer',
        'is_orderable' => 'boolean',
        'category_id' => 'integer',
        'game_id' => 'integer',
        'language_id' => 'integer',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected array $filters = [
        'name',
        'description',
        'price',
        'release_date',
        'is_orderable',
        'category_id',
        'game_id',
        'language_id',
        'greater',
        'greater_or_equal',
        'less',
        'less_or_equal',
        'between',
        'not_between',
        'sort',
        'like',
    ];

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class)->withTrashed();
    }

    public function game(): BelongsTo
    {
        return $this->belongsTo(Game::class)->withTrashed();
    }

    public function language(): BelongsTo
    {
        return $this->belongsTo(Language::class)->withTrashed();
    }
}
