<?php /** @noinspection PhpUndefinedMethodInspection */

namespace App\Models;

use App\Helpers\OrderStatus;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mehradsadeghi\FilterQueryString\FilterQueryString;

/**
 * @method static filter()
 * @mixin IdeHelperOrder
 */
class Order extends Model
{
    use HasFactory, SoftDeletes;
    use FilterQueryString;

    protected $fillable = [
        'date',
        'payment',
        'discount',
        'status',
        'user_id',
    ];

    protected $casts = [
        'user_id' => 'integer',
        'date' => 'datetime:Y-m-d\TH:i:sO',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected array $filters = [
        'id',
        'date',
        'payment',
        'discount',
        'status',
        'user_id',
        'greater',
        'greater_or_equal',
        'less',
        'less_or_equal',
        'between',
        'not_between',
        'sort',
        'like',
    ];

    public function setDateAttribute($value)
    {
        $this->attributes['date'] = date('Y-m-d H:i:s', strtotime($value));
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class, 'buy')
            ->withTrashed()
            ->using(Buy::class)
            ->withPivot('quantity');
    }

    public function buys(): HasMany
    {
        return $this->hasMany(Buy::class)->withTrashed();
    }

    public function is_not_cancelled(): bool
    {
        return $this->status !== OrderStatus::CANCELLED;
    }

    public function is_cancelled(): bool
    {
        return $this->status === OrderStatus::CANCELLED;
    }
}
