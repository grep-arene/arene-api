<?php /** @noinspection PhpUndefinedMethodInspection */

namespace App\Models;

use App\Exceptions\AlreadySubscribedException;
use App\Helpers\EventStatus;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mehradsadeghi\FilterQueryString\FilterQueryString;

/**
 * @method static filter()
 * @mixin IdeHelperEvent
 */
class Event extends Model
{
    use HasFactory, SoftDeletes;
    use FilterQueryString;

    protected $fillable = [
        'name',
        'image_path',
        'description',
        'date',
        'price',
        'game_id',
        'result_path',
        'nb_place'
    ];

    protected $casts = [
        'price' => 'double',
        'nb_place' => 'integer',
        'game_id' => 'integer',
        'date' => 'datetime:Y-m-d\TH:i:sO',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected array $filters = [
        'name',
        'description',
        'date',
        'price',
        'game_id',
        'nb_place',
        'greater',
        'greater_or_equal',
        'less',
        'less_or_equal',
        'between',
        'not_between',
        'sort',
        'like',
    ];

    public function setDateAttribute($value)
    {
        $this->attributes['date'] = date('Y-m-d H:i:s', strtotime($value));
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'participant')
            ->using(Participant::class)
            ->withPivot('status')
            ->where('participant.status', '<>', EventStatus::CANCELLED)
            ->whereNull('participant.deleted_at')
            ->withTimestamps();
    }

    public function getAvailablePlaces()
    {
        $this->loadCount('users');
        $this->available_places = $this->nb_place - $this->users_count;
    }

    public function participants(): HasMany
    {
        return $this->hasMany(Participant::class)->withTrashed();
    }

    /**
     * @throws AlreadySubscribedException
     */
    public function addParticipant(User $user): void
    {
        if ($this->is_participant($user)) {
            throw new AlreadySubscribedException();
        }
        $this->users()->attach($user->id);
    }

    public function is_participant(User $user): bool
    {
        return (bool)$this->users()
            ->where('user_id', $user->id)
            ->get()->first();
    }

    public function game(): BelongsTo
    {
        return $this->belongsTo(Game::class)->withTrashed();
    }
}
