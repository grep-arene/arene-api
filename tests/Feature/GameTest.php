<?php

namespace Tests\Feature;

use App\Exceptions\Message;
use App\Helpers\TestHelper;
use App\Models\Game;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GameTest extends TestCase
{
    const URL = '/api/games/';
    const TABLE = 'games';

    use RefreshDatabase;
    use WithFaker;

    /** @test */
    public function indexGameHappyPath()
    {
        Game::factory()->count(5)->create();

        $response = $this->getJson(self::URL);

        $response->assertOk();
        $response->assertExactJson(Game::all()->toArray());
    }

    /** @test */
    public function showGameHappyPath()
    {
        $game = Game::factory()->create();

        $response = $this->getJson(self::URL . $game->id);

        $response->assertOk();
        $response->assertExactJson($game->toArray());
    }

    /** @test */
    public function showGameIdDoesntExist()
    {
        $game = Game::factory()->create();

        $wrongId = $game->id + 100;
        $response = $this->getJson(self::URL . $wrongId);

        $response->assertNotFound();
        $response->assertExactJson(Message::FAILED_VIEW);
    }

    /** @test */
    public function storeGameHappyPath()
    {
        $expected = ['name' => $this->faker->name];
        TestHelper::logAdmin();
        $response = $this->postJson(self::URL, $expected);

        $response->assertCreated();
        $response->assertJson($expected);
        $this->assertDatabaseCount(self::TABLE, 1);
    }

    /** @test */
    public function storeGameAsClient()
    {
        TestHelper::logClient();
        $response = $this->postJson(self::URL, ['name' => $this->faker->name]);

        $response->assertForbidden();
        $this->assertDatabaseCount(self::TABLE, 0);
        $response->assertExactJson(Message::FORBIDDEN_USER);
    }

    /** @test */
    public function storeGameWithNoName()
    {
        TestHelper::logAdmin();
        $response = $this->postJson(self::URL, ['name' => '']);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertDatabaseCount(self::TABLE, 0);
        $response->assertSeeText('name');
    }

    /** @test */
    public function updateGameHappyPath()
    {
        $before = Game::factory()->create();
        $expected = ['name' => $this->faker->name];

        TestHelper::logClient();
        $response = $this->postJson(self::URL, $expected);
        $after = Game::find($before->id);

        $response->assertForbidden();
        $this->assertEquals($before->toArray(), $after->toArray());
        $response->assertSeeText(Message::FORBIDDEN_USER);
    }

    /** @test */
    public function updateGameDoesntExist()
    {
        $before = Game::factory()->create();
        $expected = ['name' => $this->faker->name];
        $wrong_id = $before->id + 100;

        TestHelper::logAdmin();
        $response = $this->putJson(self::URL . $wrong_id, $expected);
        $after = Game::find($before->id);

        $response->assertNotFound();
        $response->assertJson(Message::FAILED_UPDATE);
        $this->assertEquals($before->toArray(), $after->toArray());
    }

    /** @test */
    public function updateGameWithNoName()
    {
        $expected = Game::factory()->create();

        TestHelper::logAdmin();
        $response = $this->putJson(self::URL . $expected->id, ['name' => '']);
        $actual = Game::find($expected->id);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertSeeText('name');
        $this->assertEquals($actual->toArray(), $expected->toArray());
    }

    /** @test */
    public function deleteGameHappyPath()
    {
        $game = Game::factory()->create();

        TestHelper::logAdmin();
        $response = $this->deleteJson(self::URL . $game->id);

        $response->assertOk();
        $this->assertSoftDeleted($game);
    }

    /** @test */
    public function deleteGameAsClient()
    {
        $game = Game::factory()->create();

        TestHelper::logClient();
        $response = $this->deleteJson(self::URL . $game->id);

        $response->assertForbidden();
        $this->assertNotSoftDeleted($game);
        $response->assertJson(Message::FORBIDDEN_USER);
    }

    /** @test */
    public function deleteGameDoesntExist()
    {
        $game = Game::factory()->create();
        $wrong_id = $game->id + 100;

        TestHelper::logAdmin();
        $response = $this->deleteJson(self::URL . $wrong_id);

        $response->assertNotFound();
        $this->assertNotSoftDeleted($game);
        $response->assertJson(Message::FAILED_DELETED);
    }
}
