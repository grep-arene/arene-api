<?php

namespace Tests\Feature;

use App\Exceptions\Message;
use App\Helpers\EventStatus;
use App\Helpers\TestHelper;
use App\Models\Event;
use App\Models\Game;
use App\Models\Order;
use App\Models\Product;
use App\Models\Membership;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class UserTest extends TestCase
{
    const URL = '/api/users/';
    const TABLE = 'users';

    use RefreshDatabase;
    use WithFaker;

    /** @test */
    public function userIndexFullHappyPath()
    {
        User::factory()->count(5)->create();

        TestHelper::logAdmin();
        $response = $this->getJson(self::URL);

        $response->assertOk();
        $response->assertExactJson(User::paginate(20)->toArray());
    }

    /** @test */
    public function userIndexAsClient()
    {
        User::factory()->count(5)->create();

        $response = $this->getJson(self::URL);

        $response->assertUnauthorized();
        $response->assertExactJson(Message::UNAUTHENTICATED);
    }

    /** @test */
    public function showUserHappyPath()
    {
        $user = User::factory()->create();

        TestHelper::logAdmin();
        $response = $this->getJson(self::URL . $user->id);
        $user->refresh()->load('membership');

        $response->assertOk();
        $response->assertExactJson($user->toArray());
    }

    /** @test */
    public function showUserAsClientHappyPath()
    {
        $user = User::factory()->create();

        TestHelper::logUser($user);
        $response = $this->getJson(self::URL . $user->id);
        $user->refresh()->load('membership');

        $response->assertOk();
        $response->assertExactJson($user->toArray());
    }

    /** @test */
    public function showUserAsAnotherClient()
    {
        $user = User::factory()->create();

        TestHelper::logClient();
        $response = $this->getJson(self::URL . $user->id);

        $response->assertForbidden();
        $response->assertExactJson(Message::FORBIDDEN_USER);
    }

    /** @test */
    public function showUserIdDoesntExist()
    {
        $user = User::factory()->create();

        $wrongId = $user->id + 100;
        TestHelper::logAdmin();
        $response = $this->getJson(self::URL . $wrongId);

        $response->assertNotFound();
        $response->assertExactJson(Message::FAILED_VIEW);
    }

    /** @test */
    public function getUserOrdersHappyPath()
    {
        $user = User::factory()->create();
        Order::factory()
            ->count(3)
            ->for($user)
            ->hasAttached(Product::factory()
                ->count(3)
                ->state(['stock' => 10]),
                ['quantity' => 5])
            ->create();

        TestHelper::logUser($user);
        $response = $this->getJson(self::URL . $user->id . '/orders/');
        $actual = User::first();

        $response->assertOk();
        $response->assertExactJson($actual->orders->toArray());
    }

    /** @test */
    public function createHappyPath()
    {
        $expected = [
            'name' => $this->faker->name,
            'nickname' => $this->faker->unique()->word,
            'email' => $this->faker->unique()->safeEmail,
            'password' => $this->faker->password,
            'is_admin' => true,
            'address' => $this->faker->address,
            'phone' => $this->faker->phoneNumber,
        ];

        TestHelper::logAdmin();
        $response = $this->postJson(self::URL, $expected);
        $actual = User::whereEmail($expected['email'])->first();

        $response->assertCreated();
        $this->assertTrue(Hash::check($expected['password'], $actual->password));
        unset($expected['password']);

        $response->assertJson($expected);
        $this->assertDatabaseCount(self::TABLE, 2);
    }

    /** @test */
    public function updateUserAsAdminHappyPath()
    {
        $this->withoutExceptionHandling();
        $user = User::factory()->admin()->create();
        $expected = [
            'name' => $this->faker->name,
            'nickname' => $this->faker->unique()->word,
            'email' => $this->faker->unique()->safeEmail,
            'password' => $this->faker->password,
            'is_admin' => true,
            'address' => $this->faker->address,
            'phone' => $this->faker->phoneNumber,
        ];

        TestHelper::logAdmin();
        $response = $this->putJson(self::URL . $user->id, $expected);
        $user->refresh();

        $response->assertOk();
        $this->assertTrue(Hash::check($expected['password'], $user->password));
        unset($expected['password']);

        $response->assertJson($expected);
        $response->assertExactJson($user->toArray());
    }

    /** @test */
    public function updateUserAsClientHappyPath()
    {
        $user = User::factory()->for(Membership::factory())->create();
        $expected = [
            'name' => $this->faker->name,
            'nickname' => $this->faker->unique()->word,
            'email' => $this->faker->unique()->safeEmail,
            'password' => $this->faker->password,
            'is_admin' => false,
            'address' => $this->faker->address,
            'phone' => $this->faker->phoneNumber,
        ];

        TestHelper::logUser($user);
        $response = $this->putJson(self::URL . $user->id, $expected);
        $user->refresh();

        $response->assertOk();
        $this->assertTrue(Hash::check($expected['password'], $user->password));
        unset($expected['password']);
        $response->assertJson($expected);
        $response->assertExactJson($user->toArray());
    }

    /** @test */
    public function updatePlayersHappyPath()
    {
        $user = User::factory()->hasAttached(
            Game::factory()->count(2),
            ['player_id' => $this->faker->word]
        )->create();
        $expected = [
            'name' => $this->faker->name,
            'nickname' => $this->faker->unique()->word,
            'email' => $this->faker->unique()->safeEmail,
            'is_admin' => false,
            'address' => $this->faker->address,
            'phone' => $this->faker->phoneNumber,
        ];

        $games = Game::factory()->count(3)->create();
        $player_ids = [];
        foreach ($games as $game) {
            $player_ids[$game->id] = ['player_id' => $this->faker->word];
        }
        $expected['games'] = $player_ids;

        TestHelper::logUser($user);
        $response = $this->putJson(self::URL . $user->id, $expected);
        $user->refresh();

        $response->assertOk();
        unset($expected['games']);
        $response->assertJson($expected);
        $response->assertExactJson($user->toArray());
        foreach ($user->games as $game) {
            $this->assertEquals($player_ids[$game->id]['player_id'], $game->pivot->player_id);
        }
    }

    /** @test */
    public function updateEventsHappyPath()
    {
        $user = User::factory()->hasAttached(
            Event::factory()->count(2),
            ['status' => EventStatus::UNPAID]
        )->create();
        $expected = [
            'name' => $this->faker->name,
            'nickname' => $this->faker->unique()->word,
            'email' => $this->faker->unique()->safeEmail,
            'is_admin' => false,
            'address' => $this->faker->address,
            'phone' => $this->faker->phoneNumber,
        ];

        $events = Event::factory()->count(3)->create();
        $statuses = [];
        foreach ($events as $event) {
            $statuses[$event->id] = ['status' => EventStatus::PAID];
        }
        $expected['events'] = $statuses;

        TestHelper::logAdmin();
        $response = $this->putJson(self::URL . $user->id, $expected);
        $user->refresh();

        $response->assertOk();
        unset($expected['events']);
        $response->assertJson($expected);
        $response->assertExactJson($user->toArray());
        foreach ($user->events as $event) {
            $this->assertEquals(EventStatus::PAID, $event->pivot->status);
        }
    }

    /** @test */
    public function updateUserNoPasswordHappyPath()
    {
        $user = User::factory()->for(Membership::factory())->create();
        $expected = [
            'name' => $this->faker->name,
            'nickname' => $this->faker->unique()->word,
            'email' => $this->faker->unique()->safeEmail,
            'address' => $this->faker->address,
            'phone' => $this->faker->phoneNumber,
        ];

        TestHelper::logUser($user);
        $response = $this->putJson(self::URL . $user->id, $expected);
        $user->refresh();

        $response->assertOk();
        $response->assertJson($expected);
        $response->assertExactJson($user->toArray());
    }

    /** @test */
    public function updateUserPasswordEmptyHappyPath()
    {
        $user = User::factory()->for(Membership::factory())->create();
        $expected = [
            'name' => $this->faker->name,
            'nickname' => $this->faker->unique()->word,
            'email' => $this->faker->unique()->safeEmail,
            'password' => '',
            'address' => $this->faker->address,
            'phone' => $this->faker->phoneNumber,
        ];

        TestHelper::logUser($user);
        $response = $this->putJson(self::URL . $user->id, $expected);
        $user->refresh();

        $response->assertOk();
        unset($expected['password']);
        $response->assertJson($expected);
        $response->assertExactJson($user->toArray());
    }

    /** @test */
    public function updateTypeAsAdminHappyPath()
    {
        $user = User::factory()->for(Membership::factory())->create();
        $expected = [
            'name' => $this->faker->name,
            'nickname' => $this->faker->unique()->word,
            'email' => $this->faker->unique()->safeEmail,
            'is_admin' => true,
            'address' => $this->faker->address,
            'phone' => $this->faker->phoneNumber,
        ];

        TestHelper::logAdmin();
        $response = $this->putJson(self::URL . $user->id, $expected);
        $user->refresh();

        $response->assertOk();
        $response->assertJson($expected);
        $response->assertExactJson($user->toArray());
    }

    /** @test */
    public function updateTypeAsClientSadPath()
    {
        $user = User::factory()->for(Membership::factory())->create();
        $expected = [
            'name' => $this->faker->name,
            'nickname' => $this->faker->unique()->word,
            'email' => $this->faker->unique()->safeEmail,
            'is_admin' => true,
            'address' => $this->faker->address,
            'phone' => $this->faker->phoneNumber,
        ];
        TestHelper::logUser($user);
        $response = $this->putJson(self::URL . $user->id, $expected);
        $user->refresh();

        $response->assertOk();
        $this->assertFalse($user->is_admin);
        unset($expected['is_admin']);
        $response->assertJson($expected);
        $response->assertExactJson($user->toArray());
    }

    /** @test */
    public function updateUserWithSameEmail()
    {
        $email = $this->faker->email;
        $user = User::factory()->create(['email' => $email]);
        $expected = [
            'name' => $this->faker->name,
            'nickname' => $this->faker->unique()->word,
            'email' => $email,
            'address' => $this->faker->address,
            'phone' => $this->faker->phoneNumber,
        ];

        TestHelper::logUser($user);
        $response = $this->putJson(self::URL . $user->id, $expected);
        $user->refresh();

        $response->assertOk();
        $response->assertJson($expected);
        $response->assertExactJson($user->toArray());
    }

    /** @test */
    public function updateUserWithSameNickname()
    {
        $nickname = $this->faker->email;
        $user = User::factory()->create(['nickname' => $nickname]);
        $expected = [
            'name' => $this->faker->name,
            'nickname' => $nickname,
            'email' => $this->faker->unique()->email,
            'address' => $this->faker->address,
            'phone' => $this->faker->phoneNumber,
        ];

        TestHelper::logUser($user);
        $response = $this->putJson(self::URL . $user->id, $expected);
        $user->refresh();

        $response->assertOk();
        $response->assertJson($expected);
        $response->assertExactJson($user->toArray());
    }

    /** @test */
    public function updateAnotherUser()
    {
        $expected = User::factory()
            ->for(Membership::factory())
            ->create();

        TestHelper::logClient();
        $response = $this->putJson(self::URL . $expected->id, [
            'name' => $this->faker->name,
            'nickname' => $this->faker->unique()->word,
            'email' => $this->faker->unique()->email,
            'address' => $this->faker->address,
            'phone' => $this->faker->phoneNumber,
        ]);
        $actual = User::find($expected->id);

        $response->assertForbidden();
        $this->assertEquals($expected->toArray(), $actual->toArray());
    }

    /** @test */
    public function deleteAsClientHappyPath()
    {
        $user = User::factory()->create();

        TestHelper::logUser($user);
        $response = $this->deleteJson(self::URL . $user->id);

        $response->assertOk();
        $this->assertSoftDeleted($user);
    }

    /** @test */
    public function deleteAnotherUserSadPath()
    {
        $user = User::factory()->create();

        TestHelper::logClient();
        $response = $this->deleteJson(self::URL . $user->id);

        $response->assertForbidden();
        $this->assertNotSoftDeleted($user);
        $response->assertJson(Message::FORBIDDEN_USER);
    }

    /** @test */
    public function deleteDoesntExist()
    {
        $user = User::factory()->create();
        $user_id = $user->id + 100;

        TestHelper::logUser($user);
        $response = $this->deleteJson($this::URL . $user_id);

        $response->assertNotFound();
        $this->assertNotSoftDeleted($user);
        $response->assertJson(Message::FAILED_DELETED);
    }
}
