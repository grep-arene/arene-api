<?php

namespace Tests\Feature;

use App\Exceptions\Message;
use App\Helpers\TestHelper;
use App\Models\Membership;
use App\Models\User;
use DateTimeInterface;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class MemberTest extends TestCase
{
    const URL = '/api/members/';

    use RefreshDatabase;
    use WithFaker;

    /** @test */
    public function indexMemberHappyPath()
    {
        User::factory()
            ->count(5)
            ->state(['end_membership' => null])
            ->create();
        User::factory()
            ->for(Membership::factory())
            ->count(5)
            ->create();

        TestHelper::logAdmin();
        $response = $this->getJson(self::URL);
        $users = User::members()->paginate(20);

        $response->assertOk();
        $response->assertExactJson($users->toArray());
        $this->assertCount(5, $users);
    }

    /** @test */
    public function showMemberHappyPath()
    {
        $expected = User::factory()
            ->for(Membership::factory())
            ->create();

        TestHelper::logAdmin();
        $response = $this->getJson(self::URL . $expected->id);

        $response->assertOk();
        $response->assertExactJson($expected->toArray());
    }

    /** @test */
    public function showMemberAsClientHappyPath()
    {
        $expected = User::factory()
            ->for(Membership::factory())
            ->create();

        TestHelper::logUser($expected);
        $response = $this->getJson(self::URL . $expected->id);

        $response->assertOk();
        $response->assertExactJson($expected->toArray());
    }

    /** @test */
    public function showMemberAsAnotherClient()
    {
        $user = User::factory()
            ->for(Membership::factory())
            ->create();

        TestHelper::logClient();
        $response = $this->getJson(self::URL . $user->id);

        $response->assertForbidden();
        $response->assertExactJson(Message::FORBIDDEN_USER);
    }

    /** @test */
    public function showMemberIdDoesntExist()
    {
        $user = User::factory()
            ->for(Membership::factory())
            ->create();

        $wrongId = $user->id + 100;
        TestHelper::logAdmin();
        $response = $this->getJson(self::URL . $wrongId);

        $response->assertNotFound();
        $response->assertExactJson(Message::FAILED_VIEW);
    }

    /** @test */
    public function storeMemberHappyPath()
    {
        $user = User::factory()->create(['end_membership' => null]);
        $membership = Membership::factory()->create();
        $expected = [
            'end_membership' => $this->faker->dateTime->format(DateTimeInterface::ISO8601),
            'membership_id' => $membership->id,
        ];

        TestHelper::logAdmin();
        $response = $this->putJson(self::URL . $user->id, $expected);
        $user->refresh()->load('membership');

        $response->assertOk();
        $response->assertJson($expected);
        $response->assertExactJson($user->toArray());
    }

    /** @test */
    public function storeMemberAsClientSadPath()
    {
        $before = User::factory()->create(['end_membership' => null]);
        $membership = Membership::factory()->create();
        $expected = [
            'end_membership' => $this->faker->dateTime->format(DateTimeInterface::ISO8601),
            'membership_id' => $membership->id,
        ];

        TestHelper::logUser($before);
        $response = $this->putJson(self::URL . $before->id, $expected);
        $actual = User::find($before->id);

        $response->assertForbidden();
        $this->assertEquals($before->end_membership, $actual->end_membership);
        $this->assertEquals($before->membership_id, $actual->membership_id);
        $this->assertNotEquals($expected['end_membership'], $actual->end_membership);
        $this->assertNotEquals($expected['membership_id'], $actual->membership_id);
        $response->assertForbidden();
        $response->assertJson(Message::FORBIDDEN_USER);
    }

    /** @test */
    public function updateMemberAsAdminHappyPath()
    {
        $membership = Membership::factory()->create();
        $user = User::factory()->admin()->create();
        $expected = [
            'membership_id' => $membership->id,
            'end_membership' => $this->faker->dateTime->format(DateTimeInterface::ISO8601),
        ];

        TestHelper::logAdmin();
        $response = $this->putJson(self::URL . $user->id, $expected);
        $user->refresh()->load('membership');

        $response->assertOk();
        $response->assertJson($expected);
        $response->assertExactJson($user->toArray());
    }

    /** @test */
    public function updateMemberAsClient()
    {
        $membership = Membership::factory()->create();
        $user = User::factory()->create();

        TestHelper::logUser($user);
        $response = $this->putJson(self::URL . $user->id, [
            'membership_id' => $membership->id,
            'end_membership' => $this->faker->dateTime->format(DateTimeInterface::ISO8601),
        ]);
        $user->refresh()->load('membership');

        $response->assertForbidden();
        $response->assertJson(Message::FORBIDDEN_USER);
    }
}
