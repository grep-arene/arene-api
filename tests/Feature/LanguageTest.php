<?php

namespace Tests\Feature;

use App\Exceptions\Message;
use App\Helpers\TestHelper;
use App\Models\Language;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class LanguageTest extends TestCase
{
    const URL = '/api/languages/';
    const TABLE = 'languages';

    use RefreshDatabase;
    use WithFaker;

    /** @test */
    public function indexHappyPath()
    {
        Language::factory()->count(5)->create();

        $response = $this->getJson(self::URL);

        $response->assertOk();
        $response->assertExactJson(Language::all()->toArray());
    }

    /** @test */
    public function showLanguageHappyPath()
    {
        $language = Language::factory()->create();

        $response = $this->getJson(self::URL . $language->id);

        $response->assertOk();
        $response->assertExactJson($language->toArray());
    }

    /** @test */
    public function showLanguageIdDoesntExist()
    {
        $language = Language::factory()->create();

        $wrongId = $language->id + 100;
        $response = $this->getJson(self::URL . $wrongId);

        $response->assertNotFound();
        $response->assertExactJson(Message::FAILED_VIEW);
    }

    /** @test */
    public function storeLanguageHappyPath()
    {
        $expected = ['name' => $this->faker->languageCode];

        TestHelper::logAdmin();
        $response = $this->postJson(self::URL, $expected);

        $response->assertCreated();
        $response->assertJson($expected);
        $this->assertDatabaseCount(self::TABLE, 1);
    }

    /** @test */
    public function storeLanguageAsClient()
    {
        TestHelper::logClient();
        $response = $this->postJson(self::URL, ['name' => $this->faker->languageCode]);

        $response->assertForbidden();
        $this->assertDatabaseCount(self::TABLE, 0);
        $response->assertExactJson(Message::FORBIDDEN_USER);
    }

    /** @test */
    public function storeLanguageWithNoName()
    {
        TestHelper::logAdmin();
        $response = $this->postJson(self::URL, ['name' => '']);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertDatabaseCount(self::TABLE, 0);
        $response->assertSeeText('name');
    }

    /** @test */
    public function updateLanguageHappyPath()
    {
        $category = Language::factory()->create();
        $expected = ['name' => $this->faker->word];

        TestHelper::logAdmin();
        $response = $this->putJson(self::URL . $category->id, $expected);
        $category->refresh();

        $response->assertOk();
        $response->assertJson($expected);
        $response->assertExactJson($category->toArray());
    }

    /** @test */
    public function updateLanguageASClient()
    {
        $before = Language::factory()->create();
        $expected = ['name' => $this->faker->word];

        TestHelper::logClient();
        $response = $this->putJson(self::URL . $before->id, $expected);
        $after = Language::find($before->id);

        $response->assertForbidden();
        $this->assertEquals($before->toArray(), $after->toArray());
        $response->assertSeeText(Message::FORBIDDEN_USER);
    }

    /** @test */
    public function updateLanguageDoesntExist()
    {
        $before = Language::factory()->create();
        $expected = ['name' => $this->faker->word];
        $wrong_id = $before->id + 100;

        TestHelper::logAdmin();
        $response = $this->putJson(self::URL . $wrong_id, $expected);
        $after = Language::find($before->id);

        $response->assertNotFound();
        $response->assertJson(Message::FAILED_UPDATE);
        $this->assertEquals($before->toArray(), $after->toArray());
    }

    /** @test */
    public function updateLanguageWithNoName()
    {
        $expected = Language::create(['name' => 'old language']);

        TestHelper::logAdmin();
        $response = $this->putJson(self::URL . $expected->id, ['name' => '']);
        $actual = Language::find($expected->id);

        $this->assertEquals($expected->toArray(), $actual->toArray());
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertSeeText('name');
    }

    /** @test */
    public function deleteLanguageHappyPath()
    {
        $language = Language::factory()->create();

        TestHelper::logAdmin();
        $response = $this->deleteJson(self::URL . $language->id);

        $response->assertOk();
        $this->assertSoftDeleted($language);
    }

    /** @test */
    public function deleteLanguageAsClient()
    {
        $language = Language::factory()->create();

        TestHelper::logClient();
        $response = $this->deleteJson(self::URL . $language->id);

        $response->assertForbidden();
        $this->assertNotSoftDeleted($language);
        $response->assertJson(Message::FORBIDDEN_USER);
    }

    /** @test */
    public function deleteLanguageDoesntExist()
    {
        $language = Language::factory()->create();
        $wrong_id = $language->id + 100;

        TestHelper::logAdmin();
        $response = $this->deleteJson(self::URL . $wrong_id);

        $response->assertNotFound();
        $this->assertNotSoftDeleted($language);
        $response->assertJson(Message::FAILED_DELETED);
    }
}
