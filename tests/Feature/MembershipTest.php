<?php

namespace Tests\Feature;

use App\Exceptions\Message;
use App\Helpers\TestHelper;
use App\Models\Membership;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class MembershipTest extends TestCase
{
    const URL = '/api/memberships/';
    const TABLE = 'memberships';

    use RefreshDatabase;
    use WithFaker;

    /** @test */
    public function indexMembershipHappyPath()
    {
        Membership::factory()->count(5)->create();

        $response = $this->getJson(self::URL);

        $response->assertOk();
        $response->assertExactJson(Membership::all()->toArray());
    }

    /** @test */
    public function showMembershipHappyPath()
    {
        $membership = Membership::factory()->create();

        $response = $this->getJson(self::URL . $membership->id);

        $response->assertOk();
        $response->assertExactJson($membership->toArray());
    }

    /** @test */
    public function showMembershipIdDoesntExist()
    {
        $membership = Membership::factory()->create();
        $wrongId = $membership->id + 100;

        $response = $this->getJson(self::URL . $wrongId);

        $response->assertNotFound();
        $response->assertExactJson(Message::FAILED_VIEW);
    }

    /** @test */
    public function storeMembershipHappyPath()
    {
        $expected = [
            'name' => $this->faker->word,
            'description' => $this->faker->text,
            'price' => $this->faker->randomFloat(2, 0, 99),
        ];

        TestHelper::logAdmin();
        $response = $this->postJson(self::URL, $expected);

        $response->assertCreated();
        $response->assertJson($expected);
        $this->assertDatabaseCount(self::TABLE, 1);
    }

    /** @test */
    public function storeMembershipAsClient()
    {
        $expected = [
            'name' => $this->faker->word,
            'description' => $this->faker->text,
            'price' => $this->faker->randomFloat(2, 0, 99),
        ];

        TestHelper::logClient();
        $response = $this->postJson(self::URL, $expected);

        $response->assertForbidden();
        $this->assertDatabaseCount(self::TABLE, 0);
        $response->assertExactJson(Message::FORBIDDEN_USER);
    }

    /** @test */
    public function storeMembershipNoNameOrPrice()
    {
        $expected = [
            'name' => null,
            'description' => $this->faker->text,
            'price' => null,
        ];

        TestHelper::logAdmin();
        $response = $this->postJson(self::URL, $expected);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertSeeText('name');
        $response->assertSeeText('price');
        $this->assertDatabaseCount(self::TABLE, 0);
    }

    /** @test */
    public function updateMembershipHappyPath()
    {
        $membership = Membership::factory()->create();
        $expected = [
            'name' => $this->faker->word,
            'description' => $this->faker->text,
            'price' => $this->faker->randomFloat(2, 0, 99),
        ];

        TestHelper::logAdmin();
        $response = $this->putJson(self::URL . $membership->id, $expected);
        $membership->refresh();

        $response->assertOk();
        $response->assertJson($expected);
        $response->assertExactJson($membership->toArray());
    }

    /** @test */
    public function updateMembershipAsClient()
    {
        $before = Membership::factory()->create();
        $expected = [
            'name' => $this->faker->word,
            'description' => $this->faker->text,
            'price' => $this->faker->randomFloat(2, 0, 99),
        ];

        TestHelper::logClient();
        $response = $this->putJson(self::URL . $before->id, $expected);
        $after = Membership::find($before->id);

        $response->assertForbidden();
        $this->assertEquals($before->toArray(), $after->toArray());
        $response->assertSeeText(Message::FORBIDDEN_USER);
    }

    /** @test */
    public function updateMembershipDoesntExist()
    {
        $before = Membership::factory()->create();
        $expected = [
            'name' => $this->faker->word,
            'description' => $this->faker->text,
            'price' => $this->faker->randomFloat(2, 0, 99),
        ];
        $wrong_id = $before->id + 100;

        TestHelper::logAdmin();
        $response = $this->putJson(self::URL . $wrong_id, $expected);
        $after = Membership::find($before->id);

        $response->assertNotFound();
        $response->assertJson(Message::FAILED_UPDATE);
        $this->assertEquals($before->toArray(), $after->toArray());
    }

    /** @test */
    public function deleteMembershipHappyPath()
    {
        $membership = Membership::factory()->create();

        TestHelper::logAdmin();
        $response = $this->deleteJson(self::URL . $membership->id);

        $response->assertOk();
        $this->assertSoftDeleted($membership);
    }

    /** @test */
    public function deleteMembershipAsClient()
    {
        $membership = Membership::factory()->create();

        TestHelper::logClient();
        $response = $this->deleteJson(self::URL . $membership->id);

        $response->assertForbidden();
        $this->assertNotSoftDeleted($membership);
        $response->assertJson(Message::FORBIDDEN_USER);
    }

    /** @test */
    public function deleteMembershipDoesntExist()
    {
        $membership = Membership::factory()->create();
        $wrong_id = $membership->id + 100;

        TestHelper::logAdmin();
        $response = $this->deleteJson(self::URL . $wrong_id);

        $response->assertNotFound();
        $this->assertNotSoftDeleted($membership);
        $response->assertJson(Message::FAILED_DELETED);
    }
}
