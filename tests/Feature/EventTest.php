<?php

namespace Tests\Feature;

use App\Exceptions\Message;
use App\Helpers\EventStatus;
use App\Helpers\TestHelper;
use App\Models\Event;
use App\Models\Game;
use App\Models\User;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class EventTest extends TestCase
{
    const URL = '/api/events/';
    const TABLE = 'events';
    const URL_IS_PARTICIPANT = '/api/is-participant/';

    use RefreshDatabase;
    use WithFaker;

    /** @test */
    public function indexEventHappyPath()
    {
        Game::factory()->count(3)->create();
        Event::factory()
            ->count(5)
            ->state(new Sequence(
                fn() => ['game_id' => Game::all()->random()]
            ))
            ->create();

        $response = $this->getJson(self::URL);
        $events = Event::with('game')->get();

        $response->assertOk();
        $response->assertExactJson($events->toArray());
    }

    /** @test */
    public function showEventHappyPath()
    {
        $expected = Event::factory()
            ->for(Game::factory())
            ->create();

        $response = $this->getJson(self::URL . $expected->id);
        $expected->refresh()
            ->load('game')
            ->getAvailablePlaces();

        $response->assertOk();
        $response->assertExactJson($expected->toArray());
    }

    /** @test */
    public function showIsParticipantIsTrueHappyPath()
    {
        $user = User::factory()->create();
        $expected = Event::factory()
            ->for(Game::factory())
            ->hasAttached($user, ['status' => EventStatus::PAID])
            ->create();

        TestHelper::logUser($user);
        $response = $this->getJson(self::URL_IS_PARTICIPANT . $expected->id);
        $expected->refresh()
            ->load('game')
            ->getAvailablePlaces();
        $expected->is_participant = true;

        $response->assertOk();
        $response->assertExactJson($expected->toArray());
    }

    /** @test */
    public function showIsParticipantIsFalseHappyPath()
    {
        $user = User::factory()->create();
        $expected = Event::factory()
            ->for(Game::factory())
            ->hasAttached($user, ['status' => EventStatus::PAID])
            ->create();

        TestHelper::logClient();
        $response = $this->getJson(self::URL_IS_PARTICIPANT . $expected->id);
        $expected->refresh()
            ->load('game')
            ->getAvailablePlaces();
        $expected->is_participant = false;

        $response->assertOk();
        $response->assertExactJson($expected->toArray());
    }

    /** @test */
    public function showEventIdDoesntExist()
    {
        $event = Event::factory()->create();

        $wrongId = $event->id + 100;
        $response = $this->getJson(self::URL . $wrongId);

        $response->assertNotFound();
        $response->assertExactJson(Message::FAILED_VIEW);
    }

    /** @test */
    public function storeEventHappyPath()
    {
        $game = Game::factory()->create();
        $expected = [
            'name' => $this->faker->word,
            'date' => $this->faker->dateTime->format(DateTimeInterface::ISO8601),
            'description' => $this->faker->text,
            'price' => $this->faker->randomFloat(2, 0, 99),
            'game_id' => $game->id,
            'result_path' => null,
            'nb_place' => $this->faker->randomNumber(3),
        ];

        TestHelper::logAdmin();
        $response = $this->postJson(self::URL, $expected);

        $response->assertCreated();
        $response->assertJson($expected);
        $this->assertDatabaseCount(self::TABLE, 1);
    }

    /** @test */
    public function storeEventAsClient()
    {
        $game = Game::factory()->create();

        TestHelper::logClient();
        $response = $this->postJson(self::URL, [
            'name' => $this->faker->word,
            'date' => $this->faker->dateTime->format(DateTimeInterface::ISO8601),
            'description' => $this->faker->text,
            'price' => $this->faker->randomFloat(2, 0, 99),
            'game_id' => $game->id,
            'result_path' => null,
            'nb_place' => $this->faker->randomNumber(3),
        ]);

        $response->assertForbidden();
        $this->assertDatabaseCount(self::TABLE, 0);
        $response->assertExactJson(Message::FORBIDDEN_USER);
    }

    /** @test */
    public function storeEventWithNoName()
    {
        $game = Game::factory()->create();

        TestHelper::logAdmin();
        $response = $this->postJson(self::URL, [
            'name' => '',
            'date' => $this->faker->dateTime->format(DateTimeInterface::ISO8601),
            'description' => $this->faker->text,
            'result_path' => null,
            'game_id' => $game->id,
            'price' => $this->faker->randomFloat(2, 0, 99),
            'nb_place' => $this->faker->randomNumber(3),
        ]);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertSeeText('name');
        $this->assertDatabaseCount(self::TABLE, 0);
    }

    /** @test */
    public function storeEventWithNoDate()
    {
        $game = Game::factory()->create();

        TestHelper::logAdmin();
        $response = $this->postJson(self::URL, [
            'name' => $this->faker->word,
            'date' => '',
            'description' => $this->faker->text,
            'result_path' => null,
            'game_id' => $game->id,
            'price' => $this->faker->randomFloat(2, 0, 99),
            'nb_place' => $this->faker->randomNumber(3),
        ]);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertSeeText('date');
        $this->assertDatabaseCount(self::TABLE, 0);
    }

    /** @test */
    public function updateEventHappyPath()
    {
        $game = Game::factory()->create();
        $event = Event::factory()->create();
        $expected = [
            'name' => $this->faker->word,
            'date' => $this->faker->dateTime->format(DateTimeInterface::ISO8601),
            'description' => $this->faker->text,
            'result_path' => null,
            'game_id' => $game->id,
            'price' => $this->faker->randomFloat(2, 0, 99),
            'nb_place' => $this->faker->randomNumber(3),
        ];

        TestHelper::logAdmin();
        $response = $this->putJson(self::URL . $event->id, $expected);
        $event->refresh()->load('game');

        $response->assertOk();
        $response->assertJson($expected);
        $response->assertExactJson($event->toArray());
    }

    /** @test */
    public function updateEventAsClient()
    {
        $before = Event::factory()
            ->for(Game::factory())
            ->create(['result_path' => null]);
        $game = Game::factory()->create();
        $expected = [
            'name' => $this->faker->word,
            'date' => $this->faker->dateTime->format(DateTimeInterface::ISO8601),
            'description' => $this->faker->text,
            'result_path' => null,
            'game_id' => $game->id,
            'price' => $this->faker->randomFloat(2, 0, 99),
            'nb_place' => $this->faker->randomNumber(3),
        ];

        TestHelper::logClient();
        $response = $this->putJson(self::URL . $before->id, $expected);
        $after = Event::find($before->id);

        $response->assertForbidden();
        $this->assertEquals($before->toArray(), $after->toArray());
        $response->assertSeeText(Message::FORBIDDEN_USER);
    }


    /** @test */
    public function updateEventDoesntExist()
    {
        $before = Event::factory()
            ->for(Game::factory())
            ->create(['result_path' => null]);
        $game = Game::factory()->create();
        $expected = [
            'name' => $this->faker->word,
            'date' => $this->faker->dateTime->format(DateTimeInterface::ISO8601),
            'description' => $this->faker->text,
            'result_path' => null,
            'game_id' => $game->id,
            'price' => $this->faker->randomFloat(2, 0, 99),
            'nb_place' => $this->faker->randomNumber(3),
        ];
        $wrong_id = $before->id + 100;

        TestHelper::logAdmin();
        $response = $this->putJson(self::URL . $wrong_id, $expected);
        $after = Event::find($before->id);

        $response->assertNotFound();
        $response->assertJson(Message::FAILED_UPDATE);
        $this->assertEquals($before->toArray(), $after->toArray());
    }

    /** @test */
    public function updateParticipantsHappyPath()
    {
        $game = Game::factory()->create();
        $event = Event::factory()
            ->for($game)
            ->hasAttached(User::factory()
                ->count(2),
                ['status' => EventStatus::PAID])
            ->create();

        $expected = [
            'name' => $this->faker->word,
            'date' => $this->faker->dateTime->format(DateTimeInterface::ISO8601),
            'description' => $this->faker->text,
            'result_path' => null,
            'game_id' => $game->id,
            'price' => $this->faker->randomFloat(2, 0, 99),
            'nb_place' => $this->faker->randomNumber(3),
        ];

        $users = User::factory()->count(3)->create();
        $participants = [];
        foreach ($users as $user) {
            $participants[$user->id] = ['status' => EventStatus::getStatus()[rand(0, 2)]];
        }
        $expected['users'] = $participants;

        TestHelper::logAdmin();
        $response = $this->putJson(self::URL . $event->id, $expected);
        $event->refresh()->load('game');

        $response->assertOk();
        unset($expected['users']);
        $response->assertJson($expected);
        $response->assertExactJson($event->toArray());
        foreach ($event->users as $user) {
            $this->assertEquals($participants[$user->id]['status'], $user->pivot->status);
        }
    }

    /** @test */
    public function deleteEventHappyPath()
    {
        $event = Event::factory()->create();

        TestHelper::logAdmin();
        $response = $this->deleteJson(self::URL . $event->id);

        $response->assertOk();
        $this->assertSoftDeleted($event);
    }

    /** @test */
    public function deleteEventAsClient()
    {
        $event = Event::factory()->create();

        TestHelper::logClient();
        $response = $this->deleteJson(self::URL . $event->id);

        $response->assertForbidden();
        $this->assertNotSoftDeleted($event);
        $response->assertJson(Message::FORBIDDEN_USER);
    }

    /** @test */
    public function deleteEventDoesntExist()
    {
        $event = Event::factory()->create();
        $wrong_id = $event->id + 100;

        TestHelper::logAdmin();
        $response = $this->deleteJson(self::URL . $wrong_id);

        $response->assertNotFound();
        $this->assertNotSoftDeleted($event);
        $response->assertJson(Message::FAILED_DELETED);
    }
}
