<?php

namespace Tests\Feature;

use App\Exceptions\Message;
use App\Helpers\TestHelper;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class AuthTest extends TestCase
{
    const URL = '/api/users/';
    const URL_LOGIN = '/api/login';
    const URL_LOGIN_BACKOFFICE = '/api/login-backoffice';
    const URL_LOGOUT = '/api/logout';
    const URL_REGISTER = '/api/register';
    const URL_USER = '/api/user';
    const TABLE = 'users';

    use RefreshDatabase;
    use WithFaker;

    /** @test */
    public function loginHappyPath()
    {
        $email = $this->faker->email;
        $password = $this->faker->password;
        $user = User::factory()->create([
            'email' => $email,
            'password' => bcrypt($password),
        ]);

        $response = $this->postJson(self::URL_LOGIN, [
            'email' => $email,
            'password' => $password
        ]);

        $response->assertOk();
        $response->assertSee('access_token');
        $response->assertJson(['user' => $user->toArray()]);
    }

    /** @test */
    public function loginBackOfficeHappyPath()
    {
        $email = $this->faker->email;
        $password = $this->faker->password;
        $user = User::factory()->create([
            'email' => $email,
            'password' => bcrypt($password),
            'is_admin' => true
        ]);

        $response = $this->postJson(self::URL_LOGIN_BACKOFFICE, [
            'email' => $email,
            'password' => $password
        ]);

        $response->assertOk();
        $response->assertSee('access_token');
        $response->assertJson(['user' => $user->toArray()]);
    }

    /** @test */
    public function loginBackOfficeAsClient()
    {
        $email = $this->faker->email;
        $password = $this->faker->password;
        User::factory()->create([
            'email' => $email,
            'password' => bcrypt($password),
        ]);

        $response = $this->postJson(self::URL_LOGIN_BACKOFFICE, [
            'email' => $email,
            'password' => $password
        ]);

        $response->assertUnauthorized();
        $response->assertSeeText(Message::FAILED_LOGIN);
    }

    /** @test */
    public function loginWrongEmail()
    {
        $email = $this->faker->email;
        $password = $this->faker->password;
        User::factory()->create([
            'email' => $email,
            'password' => bcrypt($password),
        ]);

        $response = $this->postJson(self::URL_LOGIN, [
            'email' => $email . '1',
            'password' => $password,
        ]);

        $response->assertUnauthorized();
        $response->assertSeeText(Message::FAILED_LOGIN);
    }

    /** @test */
    public function loginWrongPassword()
    {
        $email = $this->faker->email;
        $password = $this->faker->password;
        User::factory()->create([
            'email' => $email,
            'password' => bcrypt($password),
        ]);

        $response = $this->postJson(self::URL_LOGIN, [
            'email' => $email,
            'password' => $password . '1',
        ]);

        $response->assertUnauthorized();
        $response->assertSeeText(Message::FAILED_LOGIN);
    }

    /** @test */
    public function registerClientHappyPath()
    {
        $expected = [
            'name' => $this->faker->name,
            'nickname' => $this->faker->unique()->word,
            'email' => $this->faker->unique()->safeEmail,
            'password' => $this->faker->password,
            'address' => $this->faker->address,
            'phone' => $this->faker->phoneNumber,
        ];

        $response = $this->postJson(self::URL_REGISTER, $expected);
        $actual = User::first();

        $response->assertCreated();
        $response->assertSee('access_token');
        $this->assertTrue(Hash::check($expected['password'], $actual->password));
        unset($expected['password']);
        $response->assertJson(['user' => $expected]);
        $this->assertDatabaseCount(self::TABLE, 1);
    }

    /** @test */
    public function registerUserWrongType()
    {
        TestHelper::logAdmin();
        $response = $this->postJson(self::URL, [
            'name' => $this->faker->name,
            'nickname' => $this->faker->unique()->name,
            'email' => $this->faker->email,
            'is_admin' => 'administrator',
            'password' => $this->faker->password,
        ]);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertSeeText('is_admin');
        //Only the registered admin is in the database
        $this->assertDatabaseCount(self::TABLE, 1);
    }

    /** @test */
    public function registerClientNoPassword()
    {
        $response = $this->postJson(self::URL_REGISTER, [
            'name' => $this->faker->name,
            'nickname' => $this->faker->unique()->name,
            'email' => $this->faker->email,
        ]);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertSeeText('password');
        $this->assertDatabaseCount(self::TABLE, 0);
    }

    /** @test */
    public function registerClientEmptyPassword()
    {
        $response = $this->postJson(self::URL_REGISTER, [
            'name' => $this->faker->name,
            'nickname' => $this->faker->unique()->name,
            'email' => $this->faker->email,
            'password' => ''
        ]);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertSeeText('password');
        $this->assertDatabaseCount(self::TABLE, 0);
    }

    /** @test */
    public function registerClientNoEmail()
    {
        $response = $this->postJson(self::URL_REGISTER, [
            'name' => $this->faker->name,
            'nickname' => $this->faker->unique()->name,
            'password' => $this->faker->password,
        ]);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertSeeText('email');
        $this->assertDatabaseCount(self::TABLE, 0);
    }

    /** @test */
    public function registerClientEmptyEmail()
    {
        $response = $this->postJson(self::URL_REGISTER, [
            'name' => $this->faker->name,
            'nickname' => $this->faker->unique()->name,
            'email' => '',
            'password' => $this->faker->password,
        ]);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertSeeText('email');
        $this->assertDatabaseCount(self::TABLE, 0);
    }

    /** @test */
    public function registerClientNoName()
    {
        $response = $this->postJson(self::URL_REGISTER, [
            'nickname' => $this->faker->unique()->name,
            'email' => $this->faker->unique()->email,
            'password' => $this->faker->password,
        ]);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertSeeText('name');
        $this->assertDatabaseCount(self::TABLE, 0);
    }

    /** @test */
    public function registerClientEmptyName()
    {
        $response = $this->postJson(self::URL_REGISTER, [
            'nickname' => $this->faker->unique()->name,
            'email' => $this->faker->unique()->email,
            'password' => $this->faker->password,
        ]);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertSeeText('name');
        $this->assertDatabaseCount(self::TABLE, 0);
    }

    /** @test */
    public function logoutAdminHappyPath()
    {
        TestHelper::logClient();
        $response = $this->postJson(self::URL_LOGOUT);

        $response->assertOk();
        $response->assertSeeText(Message::SUCCESSFUL_LOGOUT);
    }

    /** @test */
    public function logoutClientHappyPath()
    {
        TestHelper::logAdmin();
        $response = $this->postJson(self::URL_LOGOUT);

        $response->assertOk();
        $response->assertSeeText(Message::SUCCESSFUL_LOGOUT);
    }
}
