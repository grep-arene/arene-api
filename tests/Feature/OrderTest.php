<?php

namespace Tests\Feature;

use App\Exceptions\Message;
use App\Helpers\OrderStatus;
use App\Helpers\PaymentStatus;
use App\Helpers\TestHelper;
use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use DateTimeInterface;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class OrderTest extends TestCase
{
    const URL = '/api/orders/';
    const URL_BUYS = '/api/buy/';
    const URL_PAYS = '/api/pay/';
    const TABLE = 'orders';
    const PIVOT = 'buy';

    use RefreshDatabase;
    use WithFaker;

    /** @test */
    public function indexOrderFullHappyPath()
    {
        Order::factory()
            ->count(3)
            ->for(User::factory())
            ->hasAttached(Product::factory()
                ->count(3)
                ->state(['stock' => 10]),
                ['quantity' => 5])
            ->create();

        TestHelper::logAdmin();
        $response = $this->getJson(self::URL);
        $orders = Order::with(['products', 'user'])->paginate(20);

        $response->assertOk();
        $response->assertExactJson($orders->toArray());
    }

    /** @test */
    public function showOrderHappyPath()
    {
        $order = Order::factory()
            ->for(User::factory())
            ->hasAttached(Product::factory()
                ->count(3)
                ->state(['stock' => 10]),
                ['quantity' => 5])
            ->create();

        TestHelper::logAdmin();
        $response = $this->getJson(self::URL . $order->id);
        $order->refresh()->load('products', 'user');

        $response->assertOk();
        $response->assertExactJson($order->toArray());
    }

    /** @test */
    public function showOrderAsClientHappyPath()
    {
        $user = User::factory()->create();
        $order = Order::factory()
            ->for($user)
            ->hasAttached(Product::factory()
                ->count(3)
                ->state(['stock' => 10]),
                ['quantity' => 5])
            ->create();

        TestHelper::logUser($user);
        $response = $this->getJson(self::URL . $order->id);
        $order->refresh()->load('products', 'user');

        $response->assertOk();
        $response->assertExactJson($order->toArray());
    }

    /** @test */
    public function showOrderAsAnotherUser()
    {
        $order = Order::factory()
            ->for(User::factory())
            ->hasAttached(Product::factory()
                ->count(3)
                ->state(['stock' => 10]),
                ['quantity' => 5])
            ->create();

        TestHelper::logClient();
        $response = $this->getJson(self::URL . $order->id);

        $response->assertForbidden();
        $response->assertExactJson(Message::FORBIDDEN_USER);
    }

    /** @test */
    public function showOrderIdDoesntExist()
    {
        $order = Order::factory()
            ->for(User::factory())
            ->create();

        $wrongId = $order->id + 100;
        TestHelper::logClient();
        $response = $this->getJson(self::URL . $wrongId);

        $response->assertNotFound();
        $response->assertExactJson(Message::FAILED_VIEW);
    }

    /** @test */
    public function buyHappyPath()
    {
        $this->withoutExceptionHandling();
        $products = Product::factory()->count(3)->create(['stock' => 15])->toArray();

        $add_pivot = function ($product) {
            $product['pivot'] = ['quantity' => 5];
            return $product;
        };
        $expected = array_map($add_pivot, $products);

        TestHelper::logClient();
        $response = $this->postJson(self::URL_BUYS, $expected);

        $order = Order::first()
            ->load('products', 'user');

        $response->assertCreated();
        $response->assertExactJson($order->toArray());
        $response->assertCreated();
        $this->assertDatabaseCount(self::TABLE, 1);
        $this->assertDatabaseCount(self::PIVOT, 3);

        foreach (Product::all() as $product) {
            $this->assertEquals(10, $product->stock);
        }
    }

    /** @test */
    public function payOrderAsClientHappyPath()
    {
        $buyer = User::factory()->create();
        $order = Order::factory()
            ->for($buyer)
            ->state(['payment' => PaymentStatus::PENDING])
            ->hasAttached(Product::factory()
                ->count(3)
                ->state(['stock' => 10]),
                ['quantity' => 5])
            ->create();

        TestHelper::logUser($buyer);
        $response = $this->postJson(self::URL_PAYS . $order->id);
        $order->refresh()->load('products', 'user');

        $response->assertOk();
        $response->assertExactJson($order->toArray());
        $this->assertEquals(PaymentStatus::TO_BE_VERIFIED, $order->payment);
    }

    /** @test */
    public function payOrderAsAdminHappyPath()
    {
        $order = Order::factory()
            ->for(User::factory())
            ->state(['payment' => PaymentStatus::PENDING])
            ->hasAttached(Product::factory()
                ->count(3)
                ->state(['stock' => 10]),
                ['quantity' => 5])
            ->create();

        TestHelper::logAdmin();
        $response = $this->postJson(self::URL_PAYS . $order->id);
        $order->refresh()->load('products', 'user');

        $response->assertOk();
        $response->assertExactJson($order->toArray());
        $this->assertEquals(PaymentStatus::TO_BE_VERIFIED, $order->payment);
    }

    /** @test */
    public function payOrderAsAnotherClientSadPath()
    {
        $order = Order::factory()
            ->for(User::factory())
            ->state(['payment' => PaymentStatus::PENDING])
            ->hasAttached(Product::factory()
                ->count(3)
                ->state(['stock' => 10]),
                ['quantity' => 5])
            ->create();

        TestHelper::logClient();
        $response = $this->postJson(self::URL_PAYS . $order->id);
        $order->refresh()->load('products', 'user');

        $response->assertForbidden();
        $response->assertSeeText(Message::FORBIDDEN_USER);
    }

    /** @test */
    public function storeOrderHappyPath()
    {
        $expected = [
            'user_id' => User::factory()->create()->id,
            'date' => $this->faker->dateTime->format(DateTimeInterface::ISO8601),
            'payment' => PaymentStatus::PAID,
            'status' => OrderStatus::READY,
        ];

        $add_pivot = function ($product) {
            $product['pivot'] = ['quantity' => 5];
            return $product;
        };
        $products = Product::factory()->count(3)->create(['stock' => 15])->toArray();
        $expected['products'] = array_map($add_pivot, $products);

        TestHelper::logAdmin();
        $response = $this->postJson(self::URL, $expected);

        $response->assertCreated();
        unset($expected['products']);
        $response->assertJson($expected);
        $this->assertDatabaseCount(self::TABLE, 1);
        foreach (Product::all() as $product) {
            $this->assertEquals(10, $product->stock);
        }
    }

    /** @test */
    public function updateOrderHappyPath()
    {
        $order = Order::factory()
            ->for(User::factory())
            ->state(['payment' => PaymentStatus::PENDING])
            ->hasAttached(Product::factory()
                ->count(3)
                ->state(['stock' => 10]),
                ['quantity' => 5])
            ->create();

        $expected = [
            'user_id' => User::factory()->create()->id,
            'date' => $this->faker->dateTime->format(DateTimeInterface::ISO8601),
            'payment' => PaymentStatus::PAID,
            'status' => OrderStatus::READY,
        ];

        $add_pivot = function ($product) {
            $product['pivot'] = ['quantity' => 5];
            return $product;
        };
        $expected['products'] = array_map($add_pivot, Product::all()->toArray());

        TestHelper::logAdmin();
        $response = $this->putJson(self::URL . $order->id, $expected);
        $order->refresh()->load('products', 'user');

        $response->assertOk();

        $expected_products = $expected['products'];
        $actual_products = $order->products->toArray();

        for ($i = 0; $i < count($order->products); $i++) {
            $this->assertEquals($expected_products[$i]['pivot']['quantity'], $actual_products[$i]['pivot']['quantity']);
        }

        unset($expected['products']);
        $response->assertJson($expected);
        $response->assertExactJson($order->toArray());
    }

    /** @test */
    public function updateOrderAsClient()
    {
        $before = Order::factory()
            ->for(User::factory())
            ->state(['payment' => PaymentStatus::PENDING])
            ->hasAttached(Product::factory()
                ->count(3)
                ->state(['stock' => 10]),
                ['quantity' => 5])
            ->create();

        $expected = [
            'user_id' => User::factory()->create()->id,
            'date' => $this->faker->dateTime->format(DateTimeInterface::ISO8601),
            'payment' => PaymentStatus::PAID,
            'status' => OrderStatus::READY,
        ];

        $add_pivot = function ($product) {
            $product['pivot'] = ['quantity' => 5];
            return $product;
        };
        $expected['products'] = array_map($add_pivot, Product::all()->toArray());

        TestHelper::logClient();
        $response = $this->putJson(self::URL . $before->id, $expected);
        $after = Order::find($before->id);

        $response->assertForbidden();
        $this->assertEquals($before->toArray(), $after->toArray());
        $response->assertSeeText(Message::FORBIDDEN_USER);
    }

    /** @test */
    public function updateOrderDoesntExist()
    {
        $before = Order::factory()
            ->for(User::factory())
            ->state(['payment' => PaymentStatus::PENDING])
            ->hasAttached(Product::factory()
                ->count(3)
                ->state(['stock' => 10]),
                ['quantity' => 5])
            ->create();

        $expected = [
            'user_id' => User::factory()->create()->id,
            'date' => $this->faker->dateTime->format(DateTimeInterface::ISO8601),
            'payment' => PaymentStatus::PAID,
            'status' => OrderStatus::READY,
        ];

        $add_pivot = function ($product) {
            $product['pivot'] = ['quantity' => 5];
            return $product;
        };
        $expected['products'] = array_map($add_pivot, Product::all()->toArray());

        $wrong_id = $before->id + 100;

        TestHelper::logAdmin();
        $response = $this->putJson(self::URL . $wrong_id, $expected);
        $after = Order::find($before->id);

        $response->assertNotFound();
        $response->assertJson(Message::FAILED_UPDATE);
        $this->assertEquals($before->toArray(), $after->toArray());
    }

    /** @test */
    public function cancelOrderHappyPath()
    {
        $order = Order::factory()
            ->for(User::factory())
            ->state(['payment' => PaymentStatus::PENDING])
            ->hasAttached(Product::factory()
                ->count(3)
                ->state(['stock' => 10]),
                ['quantity' => 5])
            ->create();

        $expected = [
            'user_id' => User::factory()->create()->id,
            'date' => $this->faker->dateTime->format(DateTimeInterface::ISO8601),
            'payment' => PaymentStatus::PAID,
            'status' => OrderStatus::CANCELLED,
        ];

        $add_pivot = function ($product) {
            $product['pivot'] = ['quantity' => 5];
            return $product;
        };
        $expected['products'] = array_map($add_pivot, Product::all()->toArray());

        TestHelper::logAdmin();
        $response = $this->putJson(self::URL . $order->id, $expected);
        $order->refresh()->load('products', 'user');

        $response->assertOk();
        unset($expected['products']);
        $response->assertJson($expected);
        $response->assertExactJson($order->toArray());
        foreach (Product::all() as $product) {
            $this->assertEquals(15, $product->stock);
        }
    }

    /** @test */
    public function updateOrderStatusDoesntExist()
    {
        $expected = Order::factory()
            ->for(User::factory())
            ->hasAttached(Product::factory()
                ->count(3)
                ->state(['stock' => 10]),
                ['quantity' => 5])
            ->create();

        TestHelper::logAdmin();
        $response = $this->putJson(self::URL . $expected->id, ['status' => 'finished']);
        $actual = Order::find($expected->id);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertSeeText('status');
        $this->assertEquals($expected->toArray(), $actual->toArray());
    }
}
