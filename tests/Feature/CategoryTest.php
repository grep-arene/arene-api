<?php

namespace Tests\Feature;

use App\Exceptions\Message;
use App\Helpers\TestHelper;
use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class CategoryTest extends TestCase
{
    const URL = '/api/categories/';
    const TABLE = 'categories';

    use RefreshDatabase;
    use WithFaker;

    /** @test */
    public function indexCategoryHappyPath()
    {
        Category::factory()->count(5)->create();

        $response = $this->getJson(self::URL);

        $response->assertOk();
        $response->assertExactJson(Category::all()->toArray());
    }

    /** @test */
    public function showCategoryHappyPath()
    {
        $category = Category::factory()->create();

        $response = $this->getJson(self::URL . $category->id);

        $response->assertOk();
        $response->assertExactJson($category->toArray());
    }

    /** @test */
    public function showCategoryIdDoesntExist()
    {
        $category = Category::factory()->create();

        $wrongId = $category->id + 100;
        $response = $this->getJson(self::URL . $wrongId);

        $response->assertNotFound();
        $response->assertExactJson(Message::FAILED_VIEW);
    }

    /** @test */
    public function storeCategoryHappyPath()
    {
        $expected = ['name' => $this->faker->name];

        TestHelper::logAdmin();
        $response = $this->postJson(self::URL, $expected);

        $response->assertCreated();
        $response->assertJson($expected);
        $this->assertDatabaseCount(self::TABLE, 1);
    }

    /** @test */
    public function storeCategoryAsClient()
    {
        TestHelper::logClient();
        $response = $this->postJson(self::URL, ['name' => $this->faker->name]);

        $response->assertForbidden();
        $response->assertExactJson(Message::FORBIDDEN_USER);
        $this->assertDatabaseCount(self::TABLE, 0);
    }

    /** @test */
    public function storeCategoryWithNoName()
    {
        TestHelper::logAdmin();
        $response = $this->postJson(self::URL, ['name' => '']);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertSeeText('name');
        $this->assertDatabaseCount(self::TABLE, 0);
    }

    /** @test */
    public function updateCategoryHappyPath()
    {
        $category = Category::factory()->create();
        $expected = ['name' => $this->faker->word];

        TestHelper::logAdmin();
        $response = $this->putJson(self::URL . $category->id, $expected);
        $category->refresh();

        $response->assertOk();
        $response->assertJson($expected);
        $response->assertExactJson($category->toArray());
    }

    /** @test */
    public function updateCategoryAsClient()
    {
        $before = Category::factory()->create();
        $expected = ['name' => $this->faker->word];

        TestHelper::logClient();
        $response = $this->putJson(self::URL . $before->id, $expected);
        $after = Category::find($before->id);

        $response->assertForbidden();
        $this->assertEquals($before->toArray(), $after->toArray());
        $response->assertSeeText(Message::FORBIDDEN_USER);
    }

    /** @test */
    public function updateCategoryDoesntExist()
    {
        $before = Category::factory()->create();
        $expected = ['name' => $this->faker->word];
        $wrong_id = $before->id + 100;

        TestHelper::logAdmin();
        $response = $this->putJson(self::URL . $wrong_id, $expected);
        $after = Category::find($before->id);

        $response->assertNotFound();
        $response->assertJson(Message::FAILED_UPDATE);
        $this->assertEquals($before->toArray(), $after->toArray());
    }

    /** @test */
    public function updateCategoryWithNoName()
    {
        $expected = Category::factory()->create();

        TestHelper::logAdmin();
        $response = $this->putJson(self::URL . $expected->id, ['name' => '']);
        $actual = Category::find($expected->id);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertSeeText('name');
        $this->assertEquals($expected->toArray(), $actual->toArray());
    }

    /** @test */
    public function deleteCategoryHappyPath()
    {
        $category = Category::factory()->create();

        TestHelper::logAdmin();
        $response = $this->deleteJson(self::URL . $category->id);

        $response->assertOk();
        $this->assertSoftDeleted($category);
    }

    /** @test */
    public function deleteCategoryAsClient()
    {
        $category = Category::factory()->create();

        TestHelper::logClient();
        $response = $this->deleteJson(self::URL . $category->id);

        $response->assertForbidden();
        $this->assertNotSoftDeleted($category);
        $response->assertJson(Message::FORBIDDEN_USER);
    }

    /** @test */
    public function deleteCategoryDoesntExist()
    {
        $category = Category::factory()->create();
        $wrong_id = $category->id + 100;

        TestHelper::logAdmin();
        $response = $this->deleteJson(self::URL . $wrong_id);

        $response->assertNotFound();
        $this->assertNotSoftDeleted($category);
        $response->assertJson(Message::FAILED_DELETED);
    }
}
