<?php

namespace Tests\Feature;

use App\Exceptions\Message;
use App\Helpers\EventStatus;
use App\Helpers\TestHelper;
use App\Models\Event;
use App\Models\Game;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class ParticipantTest extends TestCase
{
    const URL = '/api/participants/';
    const URL_EVENTS = '/api/events/';
    const URL_USERS = '/api/users/';
    const TABLE = 'participant';

    use RefreshDatabase;
    use WithFaker;

    /** @test */
    public function addParticipantHappyPath()
    {
        $event = Event::factory()->create();
        $user = User::factory()->create();
        $expected = ['id' => $event->id];

        TestHelper::logUser($user);
        $response = $this->postJson(self::URL, $expected);

        $response->assertOk();
        $response->assertExactJson(Message::SUCCESSFUL_SUBSCRIPTION);
        $this->assertDatabaseCount(self::TABLE, 1);
    }

    /** @test */
    public function addParticipantTwiceSadPath()
    {
        $user = User::factory()->create();
        $event = Event::factory()
            ->hasAttached($user, ['status' => EventStatus::PAID])
            ->create();
        $expected = ['id' => $event->id];

        TestHelper::logUser($user);
        $response = $this->postJson(self::URL, $expected);

        $response->assertStatus(Response::HTTP_CONFLICT);
        $response->assertExactJson(Message::FAILED_SUBSCRIPTION);
        $this->assertDatabaseCount(self::TABLE, 1);
    }

    /** @test */
    public function addParticipantEventDoesntExist()
    {
        $user = User::factory()->create();
        $expected = ['id' => 100];

        TestHelper::logUser($user);
        $response = $this->postJson(self::URL, $expected);

        $response->assertNotFound();
        $response->assertExactJson(Message::FAILED_UPDATE);
        $this->assertDatabaseCount(self::TABLE, 0);
    }

    /** @test */
    public function showEventAfterSubscriptionHappyPath()
    {
        $expected = Event::factory(['nb_place' => 100])
            ->for(Game::factory())
            ->create();

        TestHelper::logUser(User::factory()->create());
        $this->postJson(self::URL, ['id' => $expected->id]);

        $response = $this->getJson(self::URL_EVENTS . $expected->id);
        $expected->refresh()
            ->load('game')
            ->getAvailablePlaces();

        $response->assertOk();
        $this->assertEquals(99, $expected->available_places);
        $response->assertExactJson($expected->toArray());
    }
}
