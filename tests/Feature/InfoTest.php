<?php

namespace Tests\Feature;

use App\Exceptions\Message;
use App\Helpers\TestHelper;
use App\Models\Info;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class InfoTest extends TestCase
{
    const URL = '/api/info/';
    const TABLE = 'infos';

    use RefreshDatabase;
    use WithFaker;

    /** @test */
    public function showInfoHappyPath()
    {
        $info = Info::factory()->create();

        $response = $this->getJson(self::URL);

        $response->assertOk();
        $response->assertExactJson($info->toArray());
    }

    /** @test */
    public function updateInfoHappyPath()
    {
        $info = Info::factory()->create();
        $expected = [
            'name' => $this->faker->name,
            'address' => $this->faker->address,
            'phone' => $this->faker->phoneNumber,
            'description' => $this->faker->text,
            'others' => $this->faker->text,
        ];

        TestHelper::logAdmin();
        $response = $this->putJson(self::URL, $expected);
        $info->refresh();

        $response->assertOk();
        $response->assertJson($expected);
        $response->assertExactJson($info->toArray());
    }

    /** @test */
    public function updateInfoAsClient()
    {
        $before = Info::factory()->create();
        $expected = [
            'name' => $this->faker->name,
            'address' => $this->faker->address,
            'phone' => $this->faker->phoneNumber,
            'description' => $this->faker->text,
            'others' => $this->faker->text,
        ];

        TestHelper::logClient();
        $response = $this->putJson(self::URL, $expected);
        $after = Info::find($before->id);

        $response->assertForbidden();
        $this->assertEquals($before->toArray(), $after->toArray());
        $response->assertSeeText(Message::FORBIDDEN_USER);
    }
}
