<?php

namespace Tests\Feature;

use App\Exceptions\Message;
use App\Helpers\TestHelper;
use App\Models\Category;
use App\Models\Language;
use App\Models\Product;
use App\Models\Game;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class ProductTest extends TestCase
{
    const URL = '/api/products/';
    const TABLE = 'products';

    use RefreshDatabase;
    use WithFaker;

    /** @test */
    public function indexHappyPath()
    {
        Category::factory()->count(5)->create();
        Game::factory()->count(5)->create();
        Language::factory()->count(5)->create();
        Product::factory()
            ->count(5)
            ->state(new Sequence(
                fn() => [
                    'category_id' => Category::all()->random(),
                    'game_id' => Game::all()->random(),
                    'language_id' => Language::all()->random(),
                ]
            ))
            ->create();

        $response = $this->getJson(self::URL);
        $products = Product::with('category', 'game', 'language')->paginate(20);

        $response->assertOk();
        $response->assertExactJson($products->toArray());
    }

    /** @test */
    public function showProductHappyPath()
    {
        $product = Product::factory()
            ->for(Category::factory())
            ->for(Game::factory())
            ->for(Language::factory())
            ->create();

        $response = $this->getJson(self::URL . $product->id);
        $product->load('category', 'game', 'language');

        $response->assertOk();
        $response->assertExactJson($product->toArray());
    }

    /** @test */
    public function showProductIdDoesntExist()
    {
        $product = Product::factory()->create();

        $wrongId = $product->id + 100;
        $response = $this->getJson(self::URL . $wrongId);

        $response->assertNotFound();
        $response->assertExactJson(Message::FAILED_VIEW);
    }

    /** @test */
    public function storeProductHappyPath()
    {
        $category = Category::factory()->create();
        $game = Game::factory()->create();
        $language = Language::factory()->create();

        $expected = [
            'name' => $this->faker->word,
            'image_path' => null,
            'description' => $this->faker->text,
            'price' => $this->faker->randomFloat(2, 0, 99),
            'stock' => $this->faker->randomNumber(3),
            'minimum_stock' => $this->faker->randomNumber(2),
            'release_date' => $this->faker->date(),
            'is_orderable' => $this->faker->boolean(),
            'category_id' => $category->id,
            'game_id' => $game->id,
            'language_id' => $language->id,
        ];

        TestHelper::logAdmin();
        $response = $this->postJson(self::URL, $expected);

        $response->assertCreated();
        $response->assertJson($expected);
        $this->assertDatabaseCount(self::TABLE, 1);
    }

    /** @test */
    public function storeProductAsClient()
    {
        $category = Category::factory()->create();
        $game = Game::factory()->create();
        $language = Language::factory()->create();

        $expected = [
            'name' => $this->faker->word,
            'image_path' => null,
            'description' => $this->faker->text,
            'price' => $this->faker->randomFloat(2, 0, 99),
            'stock' => $this->faker->randomNumber(3),
            'minimum_stock' => $this->faker->randomNumber(2),
            'release_date' => $this->faker->date(),
            'is_orderable' => $this->faker->boolean(),
            'category_id' => $category->id,
            'game_id' => $game->id,
            'language_id' => $language->id,
        ];

        TestHelper::logClient();
        $response = $this->postJson(self::URL, $expected);

        $response->assertForbidden();
        $this->assertDatabaseCount(self::TABLE, 0);
        $response->assertExactJson(Message::FORBIDDEN_USER);
    }

    /** @test */
    public function storeProductWithNoName()
    {
        TestHelper::logAdmin();
        $response = $this->postJson(self::URL, [
            'name' => '',
            'image_path' => null,
            'description' => $this->faker->text,
            'price' => $this->faker->randomFloat(2, 0, 99),
            'stock' => $this->faker->randomNumber(3),
            'minimum_stock' => $this->faker->randomNumber(2),
            'release_date' => $this->faker->date(),
            'is_orderable' => $this->faker->boolean(),
        ]);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertSeeText('name');
        $this->assertDatabaseCount(self::TABLE, 0);
    }

    /** @test */
    public function storeProductWithNoPrice()
    {
        TestHelper::logAdmin();
        $response = $this->postJson(self::URL, [
            'name' => $this->faker->word,
            'image_path' => null,
            'description' => $this->faker->text,
            'price' => null,
            'stock' => $this->faker->randomNumber(3),
            'minimum_stock' => $this->faker->randomNumber(2),
            'release_date' => $this->faker->date(),
            'is_orderable' => $this->faker->boolean(),
        ]);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertSeeText('price');
        $this->assertDatabaseCount(self::TABLE, 0);
    }


    /** @test */
    public function storeProductWithPriceAsString()
    {
        TestHelper::logAdmin();
        $response = $this->postJson(self::URL, [
            'name' => $this->faker->word,
            'description' => $this->faker->text,
            'price' => $this->faker->word,
            'stock' => $this->faker->randomNumber(3),
            'minimum_stock' => $this->faker->randomNumber(3),
            'release_date' => $this->faker->date(),
            'is_orderable' => $this->faker->boolean,
            'notifiable' => $this->faker->boolean,
        ]);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertSeeText('price');
        $this->assertDatabaseCount(self::TABLE, 0);
    }

    /** @test */
    public function storeProductWithStockAsString()
    {
        TestHelper::logAdmin();
        $response = $this->postJson(self::URL, [
            'name' => $this->faker->word,
            'description' => $this->faker->text,
            'price' => $this->faker->randomFloat(2, 0, 99),
            'stock' => $this->faker->word,
            'minimum_stock' => $this->faker->randomNumber(3),
            'release_date' => $this->faker->date(),
            'is_orderable' => $this->faker->boolean,
            'notifiable' => $this->faker->boolean,
        ]);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertSeeText('stock');
        $this->assertDatabaseCount(self::TABLE, 0);
    }

    /** @test */
    public function storeProductWithMinimumStockAsString()
    {
        TestHelper::logAdmin();
        $response = $this->postJson(self::URL, [
            'name' => $this->faker->word,
            'description' => $this->faker->text,
            'price' => $this->faker->randomFloat(2, 0, 99),
            'stock' => $this->faker->randomNumber(3),
            'minimum_stock' => $this->faker->word,
            'release_date' => $this->faker->date(),
            'is_orderable' => $this->faker->boolean,
            'notifiable' => $this->faker->boolean,
        ]);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertSeeText('minimum_stock');
        $this->assertDatabaseCount(self::TABLE, 0);
    }

    /** @test */
    public function storeProductCategoryDoesntExist()
    {
        TestHelper::logAdmin();
        $response = $this->postJson(self::URL, [
            'name' => $this->faker->word,
            'description' => $this->faker->text,
            'price' => $this->faker->randomFloat(2, 0, 99),
            'stock' => $this->faker->randomNumber(3),
            'minimum_stock' => $this->faker->word,
            'release_date' => $this->faker->date(),
            'is_orderable' => $this->faker->boolean,
            'notifiable' => $this->faker->boolean,
            'category_id' => 100,
        ]);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertSeeText('category_id');
        $this->assertDatabaseCount(self::TABLE, 0);
    }

    /** @test */
    public function storeProductGameDoesntExist()
    {
        TestHelper::logAdmin();
        $response = $this->postJson(self::URL, [
            'name' => $this->faker->word,
            'description' => $this->faker->text,
            'price' => $this->faker->randomFloat(2, 0, 99),
            'stock' => $this->faker->randomNumber(3),
            'minimum_stock' => $this->faker->word,
            'release_date' => $this->faker->date(),
            'is_orderable' => $this->faker->boolean,
            'notifiable' => $this->faker->boolean,
            'game_id' => 100,
        ]);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertSeeText('game_id');
        $this->assertDatabaseCount(self::TABLE, 0);
    }

    /** @test */
    public function storeProductLanguageDoesntExist()
    {
        TestHelper::logAdmin();
        $response = $this->postJson(self::URL, [
            'name' => $this->faker->word,
            'description' => $this->faker->text,
            'price' => $this->faker->randomFloat(2, 0, 99),
            'stock' => $this->faker->randomNumber(3),
            'minimum_stock' => $this->faker->word,
            'release_date' => $this->faker->date(),
            'is_orderable' => $this->faker->boolean,
            'notifiable' => $this->faker->boolean,
            'language_id' => 100,
        ]);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertSeeText('language_id');
        $this->assertDatabaseCount(self::TABLE, 0);
    }

    /** @test */
    public function updateHappyPath()
    {
        $category = Category::factory()->create();
        $game = Game::factory()->create();
        $language = Language::factory()->create();
        $product = Product::factory()->create();
        $expected = [
            'name' => $this->faker->word,
            'image_path' => null,
            'description' => $this->faker->text,
            'price' => $this->faker->randomFloat(2, 0, 99),
            'stock' => $this->faker->randomNumber(3),
            'minimum_stock' => $this->faker->randomNumber(2),
            'release_date' => $this->faker->date(),
            'is_orderable' => $this->faker->boolean(),
            'category_id' => $category->id,
            'game_id' => $game->id,
            'language_id' => $language->id,
        ];

        TestHelper::logAdmin();
        $response = $this->putJson(self::URL . $product->id, $expected);
        $product->refresh()->load('category', 'game', 'language');

        $response->assertOk();
        $response->assertJson($expected);
        $response->assertExactJson($product->toArray());
    }

    /** @test */
    public function updateAsClient()
    {
        $category = Category::factory()->create();
        $game = Game::factory()->create();
        $language = Language::factory()->create();
        $before = Product::factory()
            ->for(Category::factory())
            ->for(Language::factory())
            ->for(Game::factory())
            ->create();
        $expected = [
            'name' => $this->faker->word,
            'image_path' => null,
            'description' => $this->faker->text,
            'price' => $this->faker->randomFloat(2, 0, 99),
            'stock' => $this->faker->randomNumber(3),
            'minimum_stock' => $this->faker->randomNumber(2),
            'release_date' => $this->faker->date(),
            'is_orderable' => $this->faker->boolean(),
            'category_id' => $category->id,
            'game_id' => $game->id,
            'language_id' => $language->id,
        ];

        TestHelper::logClient();
        $response = $this->putJson(self::URL . $before->id, $expected);
        $after = Product::find($before->id);

        $response->assertForbidden();
        $this->assertEquals($before->toArray(), $after->toArray());
        $response->assertSeeText(Message::FORBIDDEN_USER);
    }

    /** @test */
    public function updateProductDoesntExist()
    {
        $category = Category::factory()->create();
        $game = Game::factory()->create();
        $language = Language::factory()->create();
        $before = Product::factory()
            ->for(Category::factory())
            ->for(Language::factory())
            ->for(Game::factory())
            ->create();
        $expected = [
            'name' => $this->faker->word,
            'image_path' => null,
            'description' => $this->faker->text,
            'price' => $this->faker->randomFloat(2, 0, 99),
            'stock' => $this->faker->randomNumber(3),
            'minimum_stock' => $this->faker->randomNumber(2),
            'release_date' => $this->faker->date(),
            'is_orderable' => $this->faker->boolean(),
            'category_id' => $category->id,
            'game_id' => $game->id,
            'language_id' => $language->id,
        ];
        $wrong_id = $before->id + 100;

        TestHelper::logAdmin();
        $response = $this->putJson(self::URL . $wrong_id, $expected);
        $after = Product::find($before->id);

        $response->assertNotFound();
        $response->assertJson(Message::FAILED_UPDATE);
        $this->assertEquals($before->toArray(), $after->toArray());
    }

    /** @test */
    public function deleteProductHappyPath()
    {
        $product = Product::factory()->create();

        TestHelper::logAdmin();
        $response = $this->deleteJson(self::URL . $product->id);

        $response->assertOk();
        $this->assertSoftDeleted($product);
    }

    /** @test */
    public function deleteProductAsClient()
    {
        $product = Product::factory()
            ->for(Category::factory())
            ->for(Language::factory())
            ->for(Game::factory())
            ->create();

        TestHelper::logClient();
        $response = $this->deleteJson(self::URL . $product->id);

        $response->assertForbidden();
        $this->assertNotSoftDeleted($product);
        $response->assertJson(Message::FORBIDDEN_USER);
    }

    /** @test */
    public function deleteProductDoesntExist()
    {
        $product = Product::factory()
            ->for(Category::factory())
            ->for(Language::factory())
            ->for(Game::factory())
            ->create();
        $wrong_id = $product->id + 100;

        TestHelper::logAdmin();
        $response = $this->deleteJson(self::URL . $wrong_id);

        $response->assertNotFound();
        $this->assertNotSoftDeleted($product);
        $response->assertJson(Message::FAILED_DELETED);
    }
}
