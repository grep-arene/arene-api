<?php

namespace Tests;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function assertNotSoftDeleted(Model $model): TestCase
    {
        return $this->assertDatabaseHas($model->getTable(), [
            $model->getKeyName() => $model->getKey(),
            'deleted_at' => null
        ]);
    }
}
