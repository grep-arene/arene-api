# Installation et Configuration

## Prérequis
* [PHP](https://www.php.net/downloads.php)
* [Composer](https://getcomposer.org/download/)
* [Mysql](https://dev.mysql.com/downloads/installer/)

#### Paquets natifs de PHP

Laravel utilise quelques paquets de PHP qui ne sont pas activés automatiquement quand PHP est installé, donc il faut les activer.
Il faut aller où PHP a été installé (probablement *C:/php/*) et chercher le fichier **php.ini**.

dans le fichier il faut décommenter les lignes :
```bash 
extension=fileinfo
```
```bash 
extension=pdo_mysql
```
```bash 
extension=pdo_sqlite
```

## Installation
Il suffit d'exécuter sur la racine du projet :
```bash 
composer dev:setup
```

> Attention. Il faut vérifier que le fichier généré *.env* contient les bonnes informations pour se connecter à la base de données

## Base de données
La base de données avec ses tables et ses données peuvent être créées (ou recréées) avec la commande :
```bash 
composer dev:refresh-db
```

Si vous voulez faire les étapes séparément voici les commandes individuellement.
#### Créer la base de données
```bash 
php artisan db:create
```

#### Créer les tables
```bash 
php artisan migrate
```

### Données
Pour ajouter l'utilisateur admin par défault 

```bash 
php artisan db:seed
```

## Comment faire marcher Laravel ?
Il suffit d'exécuter :

```bash 
php artisan serve
```

Laravel sera servi dans le port 8000, voici le lien pour accéder aux produits.

http://localhost:8000/api/products

Les autres liens pour d'autres ressources sont dans le fichier *routes/api.php*


## Symbolic link
Pour que le serveur puisse servir des images il faut créer un lien symbolique avec la commande suivante
```bash 
composer public_link
```

## Comment exécuter les tests ?

```bash 
php artisan test --parallel
```

## Développement

Pour générer un IDE Helper pour PHP Storm il suffit d'exécuter la commande suivante après avoir créé les tables dans la base de données.

```bash 
composer ide_helper
```


## Aide
[Documentation Laravel](https://laravel.com/docs/8.x)
